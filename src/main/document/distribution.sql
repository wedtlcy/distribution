/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50515
Source Host           : localhost:3306
Source Database       : distribution

Target Server Type    : MYSQL
Target Server Version : 50515
File Encoding         : 65001

Date: 2020-03-23 15:50:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_level_grade`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_level_grade`;
CREATE TABLE `tbl_level_grade` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `level` int(10) DEFAULT NULL COMMENT '等级',
  `levelName` varchar(60) DEFAULT NULL COMMENT '等级中文说明',
  `levelRate` decimal(20,2) DEFAULT NULL COMMENT '等级费率百分比',
  `state` varchar(60) DEFAULT NULL COMMENT '状态 normal:正常 freeze:冻结',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_level_grade
-- ----------------------------
INSERT INTO `tbl_level_grade` VALUES ('3', '1', '一级用户', '0.55', 'normal');
INSERT INTO `tbl_level_grade` VALUES ('4', '3', '三级用户', '0.05', 'normal');
INSERT INTO `tbl_level_grade` VALUES ('5', '2', '二级用户', '0.40', 'normal');

-- ----------------------------
-- Table structure for `tbl_org_day_settle`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_org_day_settle`;
CREATE TABLE `tbl_org_day_settle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orgId` bigint(20) DEFAULT NULL COMMENT '代理商id',
  `relationNo` varchar(256) DEFAULT NULL COMMENT '代理商关联',
  `settleDate` varchar(10) DEFAULT NULL COMMENT '结算日期 yyyy-MM-dd',
  `amount` decimal(12,2) DEFAULT NULL COMMENT '日分润金额',
  `transNums` int(11) DEFAULT NULL COMMENT '笔数',
  `settleStatus` varchar(10) DEFAULT NULL COMMENT '是否结算(T：结算，F未结算)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_org_day_settle
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_org_month_settle`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_org_month_settle`;
CREATE TABLE `tbl_org_month_settle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orgId` bigint(20) DEFAULT NULL COMMENT '代理商id',
  `relationNo` varchar(256) DEFAULT NULL COMMENT '代理商关联',
  `month` varchar(10) DEFAULT NULL COMMENT '结算月份',
  `amount` decimal(12,2) DEFAULT NULL COMMENT '日分润金额',
  `transNums` int(11) DEFAULT NULL COMMENT '笔数',
  `settleStatus` varchar(10) DEFAULT NULL COMMENT '是否结算(T：结算，F未结算)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_org_month_settle
-- ----------------------------
INSERT INTO `tbl_org_month_settle` VALUES ('1', '1', '1.', '2020-02', '19.40', null, 'F');
INSERT INTO `tbl_org_month_settle` VALUES ('2', '2', '1.2098789878.', '2020-02', '1.00', null, 'F');
INSERT INTO `tbl_org_month_settle` VALUES ('3', '1', '1.', '2020-02', '19.40', null, 'F');
INSERT INTO `tbl_org_month_settle` VALUES ('4', '2', '1.2098789878.', '2020-02', '1.00', null, 'F');
INSERT INTO `tbl_org_month_settle` VALUES ('5', '1', '1.', '2020-02', '19.40', null, 'F');
INSERT INTO `tbl_org_month_settle` VALUES ('6', '2', '1.2098789878.', '2020-02', '1.00', null, 'F');
INSERT INTO `tbl_org_month_settle` VALUES ('7', '1', '1.', '2020-02', '19.40', '2', 'F');
INSERT INTO `tbl_org_month_settle` VALUES ('8', '2', '1.2098789878.', '2020-02', '1.00', '4', 'F');

-- ----------------------------
-- Table structure for `tbl_org_profit`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_org_profit`;
CREATE TABLE `tbl_org_profit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orgId` bigint(20) DEFAULT NULL COMMENT '机构id',
  `createDate` varchar(60) DEFAULT NULL COMMENT '创建日期',
  `createTime` varchar(60) DEFAULT NULL COMMENT '创建时间',
  `relationNo` varchar(510) DEFAULT NULL COMMENT '机构关联',
  `fee` decimal(20,2) DEFAULT NULL COMMENT '用户费率',
  `sid` bigint(20) DEFAULT NULL COMMENT '上级用户id(顶级代理商时,sid为0)',
  `state` varchar(60) DEFAULT NULL COMMENT '状态 normal:正常 freeze:冻结',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_org_profit
-- ----------------------------
INSERT INTO `tbl_org_profit` VALUES ('2', '1', '2020-03-12', '16:28:47', '1.', '0.39', '0', 'normal');
INSERT INTO `tbl_org_profit` VALUES ('3', '2', '2020-03-12', '16:29:25', '1.2098789878.', '0.40', '1', 'normal');

-- ----------------------------
-- Table structure for `tbl_org_signle_settle`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_org_signle_settle`;
CREATE TABLE `tbl_org_signle_settle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `translsRecordId` bigint(20) DEFAULT NULL COMMENT '流水记录id',
  `orgId` bigint(20) DEFAULT NULL COMMENT '代理商id',
  `relationNo` varchar(256) DEFAULT NULL COMMENT '代理商关联',
  `settleDate` varchar(10) DEFAULT NULL COMMENT '结算日期 yyyy-MM-dd',
  `amount` decimal(12,2) DEFAULT NULL COMMENT '金额',
  `settleStatus` varchar(10) DEFAULT NULL COMMENT '是否结算(T：结算，F未结算)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_org_signle_settle
-- ----------------------------
INSERT INTO `tbl_org_signle_settle` VALUES ('1', '4', '2', '1.2.', '2020-03-12', '0.20', 'F');
INSERT INTO `tbl_org_signle_settle` VALUES ('2', '4', '1', '1.', '2020-03-12', '9.70', 'F');
INSERT INTO `tbl_org_signle_settle` VALUES ('3', '5', '2', '1.2.', '2020-03-12', '0.30', 'F');
INSERT INTO `tbl_org_signle_settle` VALUES ('4', '5', '1', '1.', '2020-03-23', '0.50', 'F');
INSERT INTO `tbl_org_signle_settle` VALUES ('5', '6', '1', '1.', '2020-03-23', '5.00', 'F');

-- ----------------------------
-- Table structure for `tbl_org_transls_day`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_org_transls_day`;
CREATE TABLE `tbl_org_transls_day` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createDate` varchar(60) DEFAULT NULL COMMENT '创建日期',
  `createTime` varchar(60) DEFAULT NULL COMMENT '创建时间',
  `orgId` bigint(20) DEFAULT NULL COMMENT '代理商id',
  `relationNo` varchar(512) DEFAULT NULL COMMENT '机构关联',
  `amount` decimal(12,2) DEFAULT NULL COMMENT '总交易金额',
  `num` int(11) DEFAULT NULL COMMENT '总笔数',
  `sid` bigint(20) DEFAULT NULL COMMENT '上级代理商id',
  `transDate` varchar(60) DEFAULT NULL COMMENT '交易日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_org_transls_day
-- ----------------------------
INSERT INTO `tbl_org_transls_day` VALUES ('1', '2020-03-13', '15:09:10', '1', '1.', '1000.00', '1', '0', '2020-02-12');
INSERT INTO `tbl_org_transls_day` VALUES ('2', '2020-03-13', '15:09:10', '2', '1.2098789878.', '1000.00', '1', '1', '2020-02-12');
INSERT INTO `tbl_org_transls_day` VALUES ('3', '2020-03-13', '15:10:10', '1', '1.', '1000.00', '1', '0', '2020-02-12');
INSERT INTO `tbl_org_transls_day` VALUES ('4', '2020-03-13', '15:10:10', '2', '1.2098789878.', '1000.00', '1', '1', '2020-02-12');

-- ----------------------------
-- Table structure for `tbl_org_transls_month`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_org_transls_month`;
CREATE TABLE `tbl_org_transls_month` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `monthYear` varchar(60) DEFAULT NULL COMMENT '统计月份',
  `orgId` bigint(20) DEFAULT NULL COMMENT '代理商id',
  `relationNo` varchar(512) DEFAULT NULL COMMENT '代理商关联',
  `amount` decimal(12,2) DEFAULT NULL COMMENT '总交易金额',
  `num` int(11) DEFAULT NULL COMMENT '总笔数',
  `sid` bigint(20) DEFAULT NULL COMMENT '上级id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_org_transls_month
-- ----------------------------
INSERT INTO `tbl_org_transls_month` VALUES ('1', '2020-02', '1', '1.', '2000.00', '2', '0');
INSERT INTO `tbl_org_transls_month` VALUES ('2', '2020-02', '2', '1.2098789878.', '2000.00', '2', '1');

-- ----------------------------
-- Table structure for `tbl_transls_day_stat`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_transls_day_stat`;
CREATE TABLE `tbl_transls_day_stat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createDate` varchar(60) DEFAULT NULL COMMENT '创建日期',
  `createTime` varchar(60) DEFAULT NULL COMMENT '创建时间',
  `transDate` varchar(60) DEFAULT NULL COMMENT '流水日期',
  `userId` bigint(20) DEFAULT NULL COMMENT '用户id',
  `urelationNo` varchar(512) DEFAULT NULL COMMENT '用户关联',
  `amount` decimal(12,2) DEFAULT NULL COMMENT '总交易金额',
  `num` int(11) DEFAULT NULL COMMENT '总笔数',
  `usid` bigint(20) DEFAULT NULL COMMENT '上级useId',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_transls_day_stat
-- ----------------------------
INSERT INTO `tbl_transls_day_stat` VALUES ('1', '2020-03-13', '12:36:36', '2020-02-12', '1', '1.', '1000.00', '2', '0');
INSERT INTO `tbl_transls_day_stat` VALUES ('2', '2020-03-13', '12:36:36', '2020-02-12', '2', '1.2.', '1000.00', '1', '1');
INSERT INTO `tbl_transls_day_stat` VALUES ('3', '2020-03-13', '12:37:30', '2020-02-12', '1', '1.', '1000.00', '1', '0');
INSERT INTO `tbl_transls_day_stat` VALUES ('4', '2020-03-13', '12:37:30', '2020-02-12', '2', '1.2.', '1000.00', '1', '1');

-- ----------------------------
-- Table structure for `tbl_transls_month_stat`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_transls_month_stat`;
CREATE TABLE `tbl_transls_month_stat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `monthYear` varchar(60) DEFAULT NULL COMMENT '统计月份',
  `userId` bigint(20) DEFAULT NULL COMMENT '用户id',
  `urelationNo` varchar(512) DEFAULT NULL COMMENT '用户关联',
  `amount` decimal(12,2) DEFAULT NULL COMMENT '总交易金额',
  `num` int(11) DEFAULT NULL COMMENT '总笔数',
  `usid` bigint(20) DEFAULT NULL COMMENT '上级useId',
  `createDate` varchar(60) DEFAULT NULL COMMENT '创建日期',
  `createTime` varchar(60) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_transls_month_stat
-- ----------------------------
INSERT INTO `tbl_transls_month_stat` VALUES ('1', '2020-02', '1', '1.', '2000.00', '3', '0', '2020-03-13', '16:12:10');
INSERT INTO `tbl_transls_month_stat` VALUES ('2', '2020-02', '2', '1.2.', '2000.00', '2', '1', '2020-03-13', '16:12:10');

-- ----------------------------
-- Table structure for `tbl_transls_record`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_transls_record`;
CREATE TABLE `tbl_transls_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createDate` varchar(60) DEFAULT NULL COMMENT '创建日期',
  `createTime` varchar(60) DEFAULT NULL COMMENT '创建时间',
  `translsId` bigint(20) DEFAULT NULL COMMENT '交易流水id',
  `transDate` varchar(60) DEFAULT NULL COMMENT '交易日期',
  `transTime` varchar(60) DEFAULT NULL COMMENT '交易时间',
  `orderNo` varchar(60) DEFAULT NULL COMMENT '订单号',
  `userId` bigint(20) DEFAULT NULL COMMENT '用户id',
  `urelationNo` varchar(512) DEFAULT NULL COMMENT '用户关联',
  `usid` bigint(20) DEFAULT NULL COMMENT '上级用户id',
  `orgId` bigint(20) DEFAULT NULL COMMENT '机构id',
  `relationNo` varchar(512) DEFAULT NULL COMMENT '机构关联',
  `sid` bigint(20) DEFAULT NULL COMMENT '上级机构id',
  `amount` decimal(12,2) DEFAULT NULL COMMENT '交易金额',
  `fee` decimal(12,2) DEFAULT NULL COMMENT '收取的总手续费',
  `isSettle` varchar(1) DEFAULT NULL COMMENT '结算状态 F:未结算 T:已结算',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_transls_record
-- ----------------------------
INSERT INTO `tbl_transls_record` VALUES ('5', '2020-03-23', '11:14:34', '1', '2019-03-05', '12:09:10', 'TF2JSN2KDM2D2', '2', '1.', '0', '1', '1.2.', '0', '1000.00', '10.00', 'F');
INSERT INTO `tbl_transls_record` VALUES ('6', '2020-03-23', '11:22:28', '1', '2019-03-05', '12:09:10', 'TF2JSN2KDM2D2', '2', '1.', '0', '1', '1.2.', '0', '1000.00', '100.00', 'T');

-- ----------------------------
-- Table structure for `tbl_user_day_settle`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_day_settle`;
CREATE TABLE `tbl_user_day_settle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) DEFAULT NULL COMMENT '用户id',
  `urelationNo` varchar(256) DEFAULT NULL COMMENT '用户关联',
  `settleDate` varchar(10) DEFAULT NULL COMMENT '结算日期 yyyy-MM-dd',
  `amount` decimal(12,2) DEFAULT NULL COMMENT '日分润金额',
  `transNums` int(11) DEFAULT NULL COMMENT '笔数',
  `settleStatus` varchar(10) DEFAULT NULL COMMENT '是否结算(T：结算，F未结算)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_user_day_settle
-- ----------------------------
INSERT INTO `tbl_user_day_settle` VALUES ('3', '1', '1.', '2020-03-13', '0.10', '1', 'T');
INSERT INTO `tbl_user_day_settle` VALUES ('4', '2', '1.2.', '2020-03-13', '0.30', '2', 'T');

-- ----------------------------
-- Table structure for `tbl_user_grade`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_grade`;
CREATE TABLE `tbl_user_grade` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userType` varchar(60) DEFAULT NULL COMMENT '用户等级(A1,A2,A3,A4)',
  `userTypeName` varchar(60) DEFAULT NULL COMMENT '用户等级中文说明',
  `userRate` decimal(20,2) DEFAULT '0.00' COMMENT '等级费率',
  `isDefault` varchar(10) DEFAULT NULL COMMENT '是否用户默认等级(用户刚创建用户的默认等级，T默认,F正常)',
  `state` varchar(60) DEFAULT NULL COMMENT '状态 normal:正常 freeze:冻结',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_user_grade
-- ----------------------------
INSERT INTO `tbl_user_grade` VALUES ('1', 'common', '普通用户', '0.40', 'T', 'normal');
INSERT INTO `tbl_user_grade` VALUES ('2', 'vip', '会员', '0.38', 'F', 'normal');

-- ----------------------------
-- Table structure for `tbl_user_month_settle`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_month_settle`;
CREATE TABLE `tbl_user_month_settle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) DEFAULT NULL COMMENT '用户id',
  `urelationNo` varchar(256) DEFAULT NULL COMMENT '用户关联',
  `month` varchar(10) DEFAULT NULL COMMENT '结算月份',
  `amount` decimal(12,2) DEFAULT NULL COMMENT '日分润金额',
  `transNums` int(11) DEFAULT NULL COMMENT '笔数',
  `settleStatus` varchar(10) DEFAULT NULL COMMENT '是否结算(T：结算，F未结算)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_user_month_settle
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_user_profit`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_profit`;
CREATE TABLE `tbl_user_profit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) DEFAULT NULL COMMENT '用户id',
  `createDate` varchar(60) DEFAULT NULL COMMENT '创建日期',
  `createTime` varchar(60) DEFAULT NULL COMMENT '创建时间',
  `userGradeId` bigint(20) DEFAULT NULL COMMENT '用户等级id',
  `urelationNo` varchar(510) DEFAULT NULL COMMENT '用户关联',
  `fee` decimal(20,2) DEFAULT NULL COMMENT '用户费率',
  `usid` bigint(20) DEFAULT NULL COMMENT '上级用户id(顶级用户时,usid为0)',
  `state` varchar(60) DEFAULT NULL COMMENT '状态 normal:正常 freeze:冻结',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_user_profit
-- ----------------------------
INSERT INTO `tbl_user_profit` VALUES ('2', '1', '2020-03-12', '14:31:21', '1', '1.', '0.42', '0', 'normal');
INSERT INTO `tbl_user_profit` VALUES ('3', '2', '2020-03-12', '17:36:00', '2', '1.2.', '0.43', '1', 'normal');
INSERT INTO `tbl_user_profit` VALUES ('4', '3', '2020-03-13', '11:25:00', '2', '1.2.3.', '0.44', '2', 'normal');

-- ----------------------------
-- Table structure for `tbl_user_signle_settle`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_signle_settle`;
CREATE TABLE `tbl_user_signle_settle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `translsRecordId` bigint(20) DEFAULT NULL COMMENT '流水记录id',
  `userId` bigint(20) DEFAULT NULL COMMENT '用户id',
  `urelationNo` varchar(256) DEFAULT NULL COMMENT '用户关联',
  `settleDate` varchar(10) DEFAULT NULL COMMENT '结算日期 yyyy-MM-dd',
  `amount` decimal(12,2) DEFAULT NULL COMMENT '金额',
  `settleStatus` varchar(10) DEFAULT NULL COMMENT '是否结算(T：结算，F未结算)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_user_signle_settle
-- ----------------------------
INSERT INTO `tbl_user_signle_settle` VALUES ('2', '4', '1', '1.', '2020-03-12', '0.10', 'F');
INSERT INTO `tbl_user_signle_settle` VALUES ('3', '4', '2', '1.2.', '2020-03-12', '0.20', 'F');
INSERT INTO `tbl_user_signle_settle` VALUES ('4', '4', '2', '1.2.', '2020-03-12', '0.10', 'F');
INSERT INTO `tbl_user_signle_settle` VALUES ('5', '5', '1', '1.', '2020-03-13', '0.30', 'F');
INSERT INTO `tbl_user_signle_settle` VALUES ('6', '5', '2', '1.2.', '2020-03-23', '5.50', 'F');
INSERT INTO `tbl_user_signle_settle` VALUES ('7', '5', '1', '1.', '2020-03-23', '4.00', 'F');
INSERT INTO `tbl_user_signle_settle` VALUES ('8', '6', '2', '1.2.', '2020-03-23', '55.00', 'F');
INSERT INTO `tbl_user_signle_settle` VALUES ('9', '6', '1', '1.', '2020-03-23', '40.00', 'F');
