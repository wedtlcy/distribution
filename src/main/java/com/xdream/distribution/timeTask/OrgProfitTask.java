package com.xdream.distribution.timeTask;

import com.alibaba.fastjson.JSONObject;
import com.xdream.distribution.entity.OrgDaySettle;
import com.xdream.distribution.entity.OrgProfit;
import com.xdream.distribution.enums.Queues;
import com.xdream.distribution.enums.SettleStatus;
import com.xdream.distribution.logic.IRabbitService;
import com.xdream.distribution.service.IOrgDaySettleService;
import com.xdream.distribution.service.IOrgProfitService;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
@Component
public class OrgProfitTask {
    private static Logger logger = Logger.getLogger(OrgProfitTask.class);
    @Resource(name="orgProfitService")
    private IOrgProfitService orgProfitService;
    @Resource(name="orgDaySettleService")
    private IOrgDaySettleService orgDaySettleService;
    @Resource(name="rabbitService")
    private IRabbitService rabbitService;

    /**
     * 代理商分润
     */
    @Scheduled(cron = "0 0 5 * * ?")
    public void task(){

        try{
            Integer day=1;
            SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
            Date beginDate = new Date();
            Calendar date = Calendar.getInstance();
            date.setTime(beginDate);
            date.set(Calendar.DATE, date.get(Calendar.DATE) - day);//LZ
            Date endDate = dft.parse(dft.format(date.getTime()));
            String settleDate=dft.format(endDate);
            //总代理商数
            Long countOrg=orgProfitService.countOrg();
            //每页条数
            String page="5000";
            //分页页数
            BigDecimal number=new BigDecimal(countOrg).divide(new BigDecimal(page),0,BigDecimal.ROUND_UP);
            Integer count=Integer.valueOf(number+"");
            System.out.println("代理商日分润流水："+count);
            for (int j = 0; j < count; j++) {
                Integer num = Integer.valueOf(page);
                Integer beginNums = num * j;
                List<OrgProfit> orgProfitList = orgProfitService.find(beginNums, num);
                if (orgProfitList == null) {
                    continue;
                }
                Long minOrgId = orgProfitList.get(0).getOrgId();
                Long maxOrgId = orgProfitList.get(orgProfitList.size() - 1).getOrgId();
                List<OrgDaySettle> orgDaySettleList=orgDaySettleService.findByDateSettle(settleDate,minOrgId,maxOrgId);
                if(orgDaySettleList==null){
                    continue;
                }
                String json= JSONObject.toJSONString(orgDaySettleList);
                //发送队列
                rabbitService.send(json, Queues.DS_ORGPROFIT.queuesCode);
                //更改日结算状态
                orgDaySettleService.updateOrgDaySettleStatus(settleDate,minOrgId,maxOrgId, SettleStatus.IN.settleCode);
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error("task=OrgProfitTask",e);
        }

    }

}
