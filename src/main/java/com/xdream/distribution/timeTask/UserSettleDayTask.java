package com.xdream.distribution.timeTask;

import com.xdream.distribution.entity.UserDaySettle;
import com.xdream.distribution.entity.UserProfit;
import com.xdream.distribution.entity.UserSingleSettle;
import com.xdream.distribution.enums.SettleStatus;
import com.xdream.distribution.service.IUserDaySettleService;
import com.xdream.distribution.service.IUserProfitService;
import com.xdream.distribution.service.IUserSingleSettleService;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class UserSettleDayTask {

    private static Logger logger = Logger.getLogger(UserSettleDayTask.class);
    @Resource(name = "userProfitService")
    private IUserProfitService userProfitService;
    @Resource(name="userSingleSettleService")
    private IUserSingleSettleService userSingleSettleService;
    @Resource(name="userDaySettleService")
    private IUserDaySettleService userDaySettleService;

    /**
     * 用户单笔转单日定时任务
     */

    @Scheduled(cron = "0 0 1 * * ?")
    public void userDaySteele(){
        try{
            Integer day=1;
            SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
            Date beginDate = new Date();
            Calendar date = Calendar.getInstance();
            date.setTime(beginDate);
            date.set(Calendar.DATE, date.get(Calendar.DATE) - day);//LZ
            Date endDate = dft.parse(dft.format(date.getTime()));
            String settleDate=dft.format(endDate);
            //总用户数
            Long countUser=userProfitService.countUser();
            //每页条数
            String page="5000";
            //分页页数
            BigDecimal number=new BigDecimal(countUser).divide(new BigDecimal(page),0,BigDecimal.ROUND_UP);
            Integer count=Integer.valueOf(number+"");
            for (int j = 0; j < count; j++) {
                Integer num=Integer.valueOf(page);
                Integer beginNums=num*j;
                List<UserProfit> userProfitList=userProfitService.find(beginNums, num);
                if (userProfitList ==null){
                    continue;
                }
                for (int i=0;i<userProfitList.size();i++){
                    Long userId=userProfitList.get(i).getUserId();
                    String urelationNo=userProfitList.get(i).getUrelationNo();
                    UserSingleSettle userSingleSettle=userSingleSettleService.findByUserId(userId,settleDate);
                    if(BigDecimal.ZERO.compareTo(userSingleSettle.getAmount())==0){
                        continue;
                    }
                    UserDaySettle userDaySettle=new UserDaySettle();
                    userDaySettle.setAmount(userSingleSettle.getAmount());
                    userDaySettle.setSettleDate(settleDate);
                    userDaySettle.setTransNums(userSingleSettle.getNums());
                    userDaySettle.setUrelationNo(urelationNo);
                    userDaySettle.setSettleStatus(SettleStatus.NOIN.settleCode);
                    userDaySettle.setUserId(userId);
                    userDaySettleService.addUserDaySettle(userDaySettle);
                }

            }

        }catch (Exception e){
            e.printStackTrace();
            logger.error("task=UserSettleDayTask",e);
        }


    }

}
