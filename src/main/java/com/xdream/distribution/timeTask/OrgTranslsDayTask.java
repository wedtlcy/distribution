package com.xdream.distribution.timeTask;

import com.xdream.distribution.entity.OrgProfit;
import com.xdream.distribution.entity.OrgTranslsDay;
import com.xdream.distribution.entity.TranslsRecord;
import com.xdream.distribution.service.IOrgProfitService;
import com.xdream.distribution.service.IOrgTranslsDayService;
import com.xdream.distribution.service.ITranslsRecordService;
import com.xdream.kernel.util.StringUtil;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class OrgTranslsDayTask {

    private static Logger logger = Logger.getLogger(OrgTranslsDayTask.class);

    @Resource(name="translsRecordService")
    private ITranslsRecordService translsRecordService;
    @Resource(name="orgProfitService")
    private IOrgProfitService orgProfitService;
    @Resource(name="orgTranslsDayService")
    private IOrgTranslsDayService orgTranslsDayService;

    /**
     * 代理商流水日统计
     */
    @Scheduled(cron = "0 0 3 * * ?")
    public void orgTranslsDay(){
        try{
            Integer day=1;
            SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
            Date beginDate = new Date();
            Calendar date = Calendar.getInstance();
            date.setTime(beginDate);
            date.set(Calendar.DATE, date.get(Calendar.DATE) - day);//LZ
            Date endDate = dft.parse(dft.format(date.getTime()));
            String settleDate=dft.format(endDate);
            //总代理商数
            Long countOrg=orgProfitService.countOrg();
            //每页条数
            String page="5000";
            //分页页数
            BigDecimal number=new BigDecimal(countOrg).divide(new BigDecimal(page),0,BigDecimal.ROUND_UP);
            Integer count=Integer.valueOf(number+"");
            System.out.println("代理商日流水统计："+count);
            for (int j = 0; j < count; j++) {
                Integer num = Integer.valueOf(page);
                Integer beginNums = num * j;
                List<OrgProfit> orgProfitList = orgProfitService.find(beginNums, num);
                if (orgProfitList == null) {
                    continue;
                }
                Long minOrgId=orgProfitList.get(0).getOrgId();
                Long maxOrgId=orgProfitList.get(orgProfitList.size()-1).getOrgId();
                List<TranslsRecord> translsRecordList=translsRecordService.sumOrgAmount(settleDate,minOrgId,maxOrgId);
                if(translsRecordList==null){
                    continue;
                }
                for (int i=0;i<translsRecordList.size();i++){
                    String createDate = StringUtil.getCurrentDateTime("yyyy-MM-dd");
                    String createTime = StringUtil.getCurrentDateTime("HH:mm:ss");
                    OrgTranslsDay orgTranslsDay=new OrgTranslsDay();
                    orgTranslsDay.setAmount(translsRecordList.get(i).getAmount());
                    orgTranslsDay.setCreateDate(createDate);
                    orgTranslsDay.setCreateTime(createTime);
                    orgTranslsDay.setNum(translsRecordList.get(i).getNums());
                    orgTranslsDay.setRelationNo(translsRecordList.get(i).getRelationNo());
                    orgTranslsDay.setSid(translsRecordList.get(i).getSid());
                    orgTranslsDay.setTransDate(settleDate);
                    orgTranslsDay.setOrgId(translsRecordList.get(i).getOrgId());
                    orgTranslsDayService.addOrgTranslsDay(orgTranslsDay);
                }

            }
        }catch (Exception e){
                e.printStackTrace();
                logger.error("task=UserTranslsDayTask",e);
            }
    }
}
