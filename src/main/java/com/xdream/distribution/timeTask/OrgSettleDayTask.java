package com.xdream.distribution.timeTask;

import com.xdream.distribution.entity.OrgDaySettle;
import com.xdream.distribution.entity.OrgProfit;
import com.xdream.distribution.entity.OrgSignleSettle;
import com.xdream.distribution.enums.SettleStatus;
import com.xdream.distribution.service.IOrgDaySettleService;
import com.xdream.distribution.service.IOrgProfitService;
import com.xdream.distribution.service.IOrgSignleSettleService;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class OrgSettleDayTask {
    private static Logger logger = Logger.getLogger(OrgSettleDayTask.class);

    @Resource(name="orgProfitService")
    private IOrgProfitService orgProfitService;
    @Resource(name="orgSignleSettleService")
    private IOrgSignleSettleService orgSignleSettleService;
    @Resource(name="orgDaySettleService")
    private IOrgDaySettleService orgDaySettleService;

    @Scheduled(cron = "0 30 1 * * ?")
    public void orgDayTask(){

        try{
            Integer day=1;
            SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
            Date beginDate = new Date();
            Calendar date = Calendar.getInstance();
            date.setTime(beginDate);
            date.set(Calendar.DATE, date.get(Calendar.DATE) - day);//LZ
            Date endDate = dft.parse(dft.format(date.getTime()));
            String settleDate=dft.format(endDate);
            //总代理商数
            Long countOrg=orgProfitService.countOrg();
            //每页条数
            String page="5000";
            //分页页数
            BigDecimal number=new BigDecimal(countOrg).divide(new BigDecimal(page),0,BigDecimal.ROUND_UP);
            Integer count=Integer.valueOf(number+"");
            System.out.println("代理商日结算："+count);
            for (int j = 0; j < count; j++) {
                Integer num=Integer.valueOf(page);
                Integer beginNums=num*j;
                List<OrgProfit> orgProfitList=orgProfitService.find(beginNums, num);
                if (orgProfitList ==null){
                    continue;
                }
               for(int i=0;i<orgProfitList.size();i++){
                    Long orgId=orgProfitList.get(i).getOrgId();
                   String relationNo=orgProfitList.get(i).getRelationNo();
                   OrgSignleSettle orgSignleSettle= orgSignleSettleService.findByOrgId(orgId,settleDate);
                   if(BigDecimal.ZERO.compareTo(orgSignleSettle.getAmount())==0){
                       continue;
                   }
                   OrgDaySettle orgDaySettle=new OrgDaySettle();
                   orgDaySettle.setSettleDate(settleDate);
                   orgDaySettle.setAmount(orgSignleSettle.getAmount());
                   orgDaySettle.setOrgId(orgId);
                   orgDaySettle.setRelationNo(relationNo);
                   orgDaySettle.setTransNums(orgSignleSettle.getNums());
                   orgDaySettle.setSettleStatus(SettleStatus.NOIN.settleCode);
                   orgDaySettleService.addOrgDaySettle(orgDaySettle);
                }

            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error("task=OrgSettleDayTask",e);
        }

    }
}
