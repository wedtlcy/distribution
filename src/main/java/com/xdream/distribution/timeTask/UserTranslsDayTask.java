package com.xdream.distribution.timeTask;

import com.xdream.distribution.entity.TranslsDayStat;
import com.xdream.distribution.entity.TranslsRecord;
import com.xdream.distribution.entity.UserProfit;
import com.xdream.distribution.service.ITranslsDayStatService;
import com.xdream.distribution.service.ITranslsRecordService;
import com.xdream.distribution.service.IUserProfitService;
import com.xdream.kernel.util.StringUtil;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class UserTranslsDayTask {
    @Resource(name = "userProfitService")
    private IUserProfitService userProfitService;
    @Resource(name="translsRecordService")
    private ITranslsRecordService translsRecordService;
    @Resource(name="translsDayStatService")
    private ITranslsDayStatService translsDayStatService;
    private static Logger logger = Logger.getLogger(UserTranslsDayTask.class);

    /**
     * 用户流水日统计
     */
    @Scheduled(cron = "0 0 2 * * ?")
    public void userDayStatic(){
        try {
            Integer day=1;
            SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
            Date beginDate = new Date();
            Calendar date = Calendar.getInstance();
            date.setTime(beginDate);
            date.set(Calendar.DATE, date.get(Calendar.DATE) - day);//LZ
            Date endDate = dft.parse(dft.format(date.getTime()));
            String settleDate=dft.format(endDate);
            //总用户数
            Long countUser=userProfitService.countUser();
            //每页条数
            String page="5000";
            //分页页数
            BigDecimal number=new BigDecimal(countUser).divide(new BigDecimal(page),0,BigDecimal.ROUND_UP);
            Integer count=Integer.valueOf(number+"");
            for (int j = 0; j < count; j++) {
                Integer num = Integer.valueOf(page);
                Integer beginNums = num * j;
                List<UserProfit> userProfitList = userProfitService.find(beginNums, num);
                if (userProfitList == null) {
                    continue;
                }
                Long minUserId=userProfitList.get(0).getUserId();
                Long maxUserId=userProfitList.get(userProfitList.size()-1).getUserId();
                List<TranslsRecord> translsRecordList=translsRecordService.sumAmount(settleDate,minUserId,maxUserId);
                if(translsRecordList==null){
                    continue;
                }
                for (int i=0;i<translsRecordList.size();i++){
                    String createDate = StringUtil.getCurrentDateTime("yyyy-MM-dd");
                    String createTime = StringUtil.getCurrentDateTime("HH:mm:ss");
                    TranslsDayStat translsDayStat=new TranslsDayStat();
                    translsDayStat.setTransDate(settleDate);
                    translsDayStat.setCreateDate(createDate);
                    translsDayStat.setAmount(translsRecordList.get(i).getAmount());
                    translsDayStat.setCreateTime(createTime);
                    translsDayStat.setNum(translsRecordList.get(i).getNums());
                    translsDayStat.setUserId(translsRecordList.get(i).getUserId());
                    translsDayStat.setUsid(translsRecordList.get(i).getUsid());
                    translsDayStat.setUrelationNo(translsRecordList.get(i).getUrelationNo());
                    translsDayStatService.addTranslsDayStat(translsDayStat);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error("task=UserTranslsDayTask",e);
        }
    }
}
