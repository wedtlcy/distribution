package com.xdream.distribution.timeTask;

import com.xdream.distribution.entity.OrgProfit;
import com.xdream.distribution.entity.OrgTranslsDay;
import com.xdream.distribution.entity.OrgTranslsMonth;
import com.xdream.distribution.service.IOrgProfitService;
import com.xdream.distribution.service.IOrgTranslsDayService;
import com.xdream.distribution.service.IOrgTranslsMonthService;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
@Component
public class OrgTranslsMonthStaticTask {
    private static Logger logger = Logger.getLogger(OrgTranslsMonthStaticTask.class);
    @Resource(name="orgProfitService")
    private IOrgProfitService orgProfitService;
    @Resource(name="orgTranslsDayService")
    private IOrgTranslsDayService orgTranslsDayService;
    @Resource(name = "orgTranslsMonthService")
    private IOrgTranslsMonthService orgTranslsMonthService;
    /**
     * 代理商流水月统计
     */
    @Scheduled(cron = "0 10 0 6 * ?")
    public  void orgTranslsMonthTask(){
        try{
            Integer month=1;
            SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM");
            Date beginDate = new Date();
            Calendar date = Calendar.getInstance();
            date.setTime(beginDate);
            date.set(Calendar.MONTH, date.get(Calendar.MONTH) - month);//LZ
            Date endDate = dft.parse(dft.format(date.getTime()));
            String settleDate=dft.format(endDate);//年月
            System.out.println(settleDate);
            //总代理商数
            Long countOrg=orgProfitService.countOrg();
            //每页条数
            String page="5000";
            //分页页数
            BigDecimal number=new BigDecimal(countOrg).divide(new BigDecimal(page),0,BigDecimal.ROUND_UP);
            Integer count=Integer.valueOf(number+"");
            System.out.println("代理商月流水统计："+count);
            for (int j = 0; j < count; j++) {
                Integer num = Integer.valueOf(page);
                Integer beginNums = num * j;
                List<OrgProfit> orgProfitList = orgProfitService.find(beginNums, num);
                if (orgProfitList == null) {
                    continue;
                }
                Long minOrgId = orgProfitList.get(0).getOrgId();
                Long maxOrgId = orgProfitList.get(orgProfitList.size() - 1).getOrgId();
                List<OrgTranslsDay> orgTranslsDayList=orgTranslsDayService.findByOrgId(settleDate,minOrgId,maxOrgId);
                if (orgTranslsDayList==null){
                    continue;
                }
                for (int i=0;i<orgTranslsDayList.size();i++){
                    OrgTranslsMonth orgTranslsMonth=new OrgTranslsMonth();
                    orgTranslsMonth.setAmount(orgTranslsDayList.get(i).getAmount());
                    orgTranslsMonth.setMonthYear(settleDate);
                    orgTranslsMonth.setNum(orgTranslsDayList.get(i).getNum());
                    orgTranslsMonth.setOrgId(orgTranslsDayList.get(i).getOrgId());
                    orgTranslsMonth.setSid(orgTranslsDayList.get(i).getSid());
                    orgTranslsMonth.setRelationNo(orgTranslsDayList.get(i).getRelationNo());
                    orgTranslsMonthService.addOrgTranslsMonth(orgTranslsMonth);
                }

            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error("task=OrgTranslsMonthStaticTask",e);

        }

    }

}
