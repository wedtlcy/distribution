package com.xdream.distribution.timeTask;

import com.xdream.distribution.entity.OrgDaySettle;
import com.xdream.distribution.entity.OrgMonthSettle;
import com.xdream.distribution.entity.OrgProfit;
import com.xdream.distribution.enums.SettleStatus;
import com.xdream.distribution.service.IOrgDaySettleService;
import com.xdream.distribution.service.IOrgMonthSettleService;
import com.xdream.distribution.service.IOrgProfitService;
import com.xdream.distribution.service.IOrgTranslsDayService;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
@Component
public class OrgSettleMonthTask {
    private static Logger logger = Logger.getLogger(OrgSettleMonthTask.class);
    @Resource(name="orgProfitService")
    private IOrgProfitService orgProfitService;
    @Resource(name="orgDaySettleService")
    private IOrgDaySettleService orgDaySettleService;
    @Resource(name="orgMonthSettleService")
    private IOrgMonthSettleService orgMonthSettleService;
    /**
     * 代理商结算月统计
     */
    @Scheduled(cron = "0 10 0 16 * ?")
    public  void task(){
      try {
          Integer month=1;
          SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM");
          Date beginDate = new Date();
          Calendar date = Calendar.getInstance();
          date.setTime(beginDate);
          date.set(Calendar.MONTH, date.get(Calendar.MONTH) - month);//LZ
          Date endDate = dft.parse(dft.format(date.getTime()));
          String settleDate=dft.format(endDate);//年月
          System.out.println(settleDate);
          //总代理商数
          Long countOrg=orgProfitService.countOrg();
          //每页条数
          String page="5000";
          //分页页数
          BigDecimal number=new BigDecimal(countOrg).divide(new BigDecimal(page),0,BigDecimal.ROUND_UP);
          Integer count=Integer.valueOf(number+"");
          System.out.println("代理商结算月统计："+count);
          for (int j = 0; j < count; j++) {
              Integer num = Integer.valueOf(page);
              Integer beginNums = num * j;
              List<OrgProfit> orgProfitList = orgProfitService.find(beginNums, num);
              if (orgProfitList == null) {
                  continue;
              }
              Long minOrgId = orgProfitList.get(0).getOrgId();
              Long maxOrgId = orgProfitList.get(orgProfitList.size() - 1).getOrgId();
              List<OrgDaySettle> orgDaySettleList=orgDaySettleService.findByDateAndOrgId(settleDate,minOrgId,maxOrgId);
              if(orgDaySettleList == null){
                  continue;
              }
              for (OrgDaySettle orgDaySettle:orgDaySettleList) {
                  OrgMonthSettle orgMonthSettle=new OrgMonthSettle();
                  orgMonthSettle.setAmount(orgDaySettle.getAmount());
                  orgMonthSettle.setMonth(settleDate);
                  orgMonthSettle.setOrgId(orgDaySettle.getOrgId());
                  orgMonthSettle.setRelationNo(orgDaySettle.getRelationNo());
                  orgMonthSettle.setTransNums(orgDaySettle.getTransNums());
                  orgMonthSettle.setSettleStatus(SettleStatus.NOIN.settleCode);
                  System.out.println(orgMonthSettle.toString());
                  orgMonthSettleService.addOrgMonthSettle(orgMonthSettle);
              }

          }
      }catch (Exception e){
          e.printStackTrace();
          logger.error("task=OrgSettleMonthTask",e);

      }
    }
}
