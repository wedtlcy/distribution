package com.xdream.distribution.timeTask;

import com.alibaba.fastjson.JSONObject;
import com.xdream.distribution.entity.UserDaySettle;
import com.xdream.distribution.entity.UserProfit;
import com.xdream.distribution.enums.Queues;
import com.xdream.distribution.enums.SettleStatus;
import com.xdream.distribution.logic.IRabbitService;
import com.xdream.distribution.service.IUserDaySettleService;
import com.xdream.distribution.service.IUserProfitService;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
@Component
public class UserProfitTask {
    private static Logger logger = Logger.getLogger(UserProfitTask.class);
    @Resource(name = "userProfitService")
    private IUserProfitService userProfitService;
    @Resource(name="userDaySettleService")
    private IUserDaySettleService userDaySettleService;
    @Resource(name="rabbitService")
    private IRabbitService rabbitService;

    /**
     * 用户分润传输
     */
    @Scheduled(cron = "0 0 4 * * ?")
    public void userProfit(){

        try {
            Integer day=1;
            SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
            Date beginDate = new Date();
            Calendar date = Calendar.getInstance();
            date.setTime(beginDate);
            date.set(Calendar.DATE, date.get(Calendar.DATE) - day);//LZ
            Date endDate = dft.parse(dft.format(date.getTime()));
            String settleDate=dft.format(endDate);
            //总用户数
            Long countUser=userProfitService.countUser();
            //每页条数
            String page="5000";
            //分页页数
            BigDecimal number=new BigDecimal(countUser).divide(new BigDecimal(page),0,BigDecimal.ROUND_UP);
            Integer count=Integer.valueOf(number+"");
            for (int j = 0; j < count; j++) {
                Integer num = Integer.valueOf(page);
                Integer beginNums = num * j;
                List<UserProfit> userProfitList = userProfitService.find(beginNums, num);
                if (userProfitList == null) {
                    continue;
                }
                Long minUserId = userProfitList.get(0).getUserId();
                Long maxUserId = userProfitList.get(userProfitList.size() - 1).getUserId();
                //获取用户日结算数据
                List<UserDaySettle> userDaySettleList=userDaySettleService.findDaySettle(settleDate,minUserId,maxUserId);
                if (userDaySettleList==null){
                    continue;
                }
                String json= JSONObject.toJSONString(userDaySettleList);
                //发送队列
                rabbitService.send(json, Queues.DS_USERPROFIT.queuesCode);
                //更改日结算状态
                userDaySettleService.updateSettleStatus(settleDate,minUserId,maxUserId, SettleStatus.IN.settleCode);
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error("task=UserProfitTask",e);
        }

    }

}
