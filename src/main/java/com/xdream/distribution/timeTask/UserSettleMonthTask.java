package com.xdream.distribution.timeTask;

import com.xdream.distribution.entity.UserDaySettle;
import com.xdream.distribution.entity.UserMonthSettle;
import com.xdream.distribution.entity.UserProfit;
import com.xdream.distribution.enums.SettleStatus;
import com.xdream.distribution.service.IUserDaySettleService;
import com.xdream.distribution.service.IUserMonthSettleService;
import com.xdream.distribution.service.IUserProfitService;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
@Component
public class UserSettleMonthTask {

    private static Logger logger = Logger.getLogger(UserSettleMonthTask.class);
    @Resource(name = "userProfitService")
    private IUserProfitService userProfitService;
    @Resource(name="userDaySettleService")
    private IUserDaySettleService userDaySettleService;
    @Resource(name="userMonthSettleService")
    private IUserMonthSettleService userMonthSettleService;
    /**
     * 用户结算月统计
     */
    @Scheduled(cron = "0 10 0 15 * ?")
    public void task(){
        try {
            Integer month=1;
            SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM");
            Date beginDate = new Date();
            Calendar date = Calendar.getInstance();
            date.setTime(beginDate);
            date.set(Calendar.MONTH, date.get(Calendar.MONTH) - month);//LZ
            Date endDate = dft.parse(dft.format(date.getTime()));
            String settleDate=dft.format(endDate);//年月
            System.out.println(settleDate);
            //总用户数
            Long countUser=userProfitService.countUser();
            //每页条数
            String page="5000";
            //分页页数
            BigDecimal number=new BigDecimal(countUser).divide(new BigDecimal(page),0,BigDecimal.ROUND_UP);
            Integer count=Integer.valueOf(number+"");
            System.out.println("用户结算月统计:"+count);
            for (int j = 0; j < count; j++) {
                Integer num = Integer.valueOf(page);
                Integer beginNums = num * j;
                List<UserProfit> userProfitList = userProfitService.find(beginNums, num);
                if (userProfitList == null) {
                    continue;
                }
                Long minUserId = userProfitList.get(0).getUserId();
                Long maxUserId = userProfitList.get(userProfitList.size() - 1).getUserId();
                List<UserDaySettle> userDaySettleList=userDaySettleService.findByDateAndUserId(settleDate,minUserId,maxUserId);
                if (userDaySettleList ==null){
                    continue;
                }
                for (int i=0;i<userDaySettleList.size();i++){
                    UserMonthSettle userMonthSettle=new UserMonthSettle();
                    userMonthSettle.setAmount(userDaySettleList.get(i).getAmount());
                    userMonthSettle.setMonth(settleDate);
                    userMonthSettle.setTransNums(userDaySettleList.get(i).getTransNums());
                    userMonthSettle.setUrelationNo(userDaySettleList.get(i).getUrelationNo());
                    userMonthSettle.setUserId(userDaySettleList.get(i).getUserId());
                    userMonthSettle.setSettleStatus(SettleStatus.NOIN.settleCode);
                    userMonthSettleService.addUserMonthSettle(userMonthSettle);
                }


            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error("task=UserSettleMonthTask",e);
        }

    }
}
