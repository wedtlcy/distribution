package com.xdream.distribution.timeTask;

import com.xdream.distribution.entity.TranslsDayStat;
import com.xdream.distribution.entity.TranslsMonthStat;
import com.xdream.distribution.entity.UserProfit;
import com.xdream.distribution.service.ITranslsDayStatService;
import com.xdream.distribution.service.ITranslsMonthStatService;
import com.xdream.distribution.service.IUserProfitService;
import com.xdream.kernel.util.StringUtil;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class UserTranslsMonthStaticTask {
    private static Logger logger = Logger.getLogger(UserTranslsMonthStaticTask.class);
    @Resource(name = "userProfitService")
    private IUserProfitService userProfitService;
    @Resource(name="translsDayStatService")
    private ITranslsDayStatService translsDayStatService;
    @Resource(name="translsMonthStatService")
    private ITranslsMonthStatService translsMonthStatService;

    /**
     * 用户流水月统计
     */
    @Scheduled(cron = "0 10 0 5 * ?")
    public  void userTranslsStaticTask(){
        try{
            Integer month=1;
            SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM");
            Date beginDate = new Date();
            Calendar date = Calendar.getInstance();
            date.setTime(beginDate);
            date.set(Calendar.MONTH, date.get(Calendar.MONTH) - month);//LZ
            Date endDate = dft.parse(dft.format(date.getTime()));
            String settleDate=dft.format(endDate);//年月
            System.out.println(settleDate);
            //总用户数
            Long countUser=userProfitService.countUser();
            //每页条数
            String page="5000";
            //分页页数
            BigDecimal number=new BigDecimal(countUser).divide(new BigDecimal(page),0,BigDecimal.ROUND_UP);
            Integer count=Integer.valueOf(number+"");
            System.out.println("用户月统计:"+count);
            for (int j = 0; j < count; j++) {
                Integer num = Integer.valueOf(page);
                Integer beginNums = num * j;
                List<UserProfit> userProfitList = userProfitService.find(beginNums, num);
                if (userProfitList == null) {
                    continue;
                }
                Long minUserId=userProfitList.get(0).getUserId();
                Long maxUserId=userProfitList.get(userProfitList.size()-1).getUserId();
                List<TranslsDayStat> translsDayStatlist=translsDayStatService.findByUserId(settleDate,minUserId,maxUserId);
                if (translsDayStatlist ==null){
                    continue;
                }
                 for (int i=0;i<translsDayStatlist.size();i++){
                     TranslsMonthStat translsMonthStat=new TranslsMonthStat();
                     String createDate = StringUtil.getCurrentDateTime("yyyy-MM-dd");
                     String createTime = StringUtil.getCurrentDateTime("HH:mm:ss");
                     translsMonthStat.setAmount(translsDayStatlist.get(i).getAmount());
                     translsMonthStat.setCreateDate(createDate);
                     translsMonthStat.setCreateTime(createTime);
                     translsMonthStat.setMonthYear(settleDate);
                     translsMonthStat.setNum(translsDayStatlist.get(i).getNum());
                     translsMonthStat.setUrelationNo(translsDayStatlist.get(i).getUrelationNo());
                     translsMonthStat.setUserId(translsDayStatlist.get(i).getUserId());
                     translsMonthStat.setUsid(translsDayStatlist.get(i).getUsid());
                     translsMonthStatService.addTranslsMonthStat(translsMonthStat);
                 }
            }

        }catch (Exception e){
            e.printStackTrace();
            logger.error("task=UserTranslsMonthStaticTask",e);

        }

    }
}
