package com.xdream.distribution.controller;

import com.xdream.distribution.entity.OrgProfit;
import com.xdream.distribution.logic.IOrgLogicService;
import com.xdream.distribution.rabbitTask.TranslsRecordListener;
import com.xdream.distribution.utils.ResultUtil;
import com.xdream.kernel.util.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.UUID;

@SuppressWarnings("serial")
@Controller
@RequestMapping(value="/org")
public class OrgController {

    private static Logger logger = Logger.getLogger(OrgController.class);
    @Resource(name="orgLogicService")
    private IOrgLogicService orgLogicService;

    /**
     * 添加代理商
     * @param request
     * @param response
     * @throws Exception
     */

    @RequestMapping(value="/add")
    public void  add(HttpServletRequest request, HttpServletResponse response)throws Exception{
          String orgId=request.getParameter("orgId");//代理商id
          String sid=request.getParameter("sid");//上级代理商id
          String relationNo=request.getParameter("relationNo");//代理商关联
          String fee=request.getParameter("fee");//代理商费率
          String resultJson="";
          if(StringUtils.isBlank(orgId)){
            resultJson= ResultUtil.sendJson("fail","代理商id不能为空",null,null);
            ResponseUtil.responseJson(response, resultJson);
            return;
        }
          if(StringUtils.isBlank(sid)){
              resultJson= ResultUtil.sendJson("fail","上级代理商id不能为空",null,null);
              ResponseUtil.responseJson(response, resultJson);
              return;
          }
          if(StringUtils.isBlank(relationNo)){
              resultJson= ResultUtil.sendJson("fail","代理商关联不能为空",null,null);
              ResponseUtil.responseJson(response, resultJson);
              return;
          }
          if(StringUtils.isBlank(fee)){
              resultJson= ResultUtil.sendJson("fail","代理商结算费率不能为空",null,null);
              ResponseUtil.responseJson(response, resultJson);
              return;
          }
          String uuid= UUID.randomUUID().toString().replace("-", "");

          try {
              logger.info("uuid->"+uuid+",method=add,params={orgId:"+orgId+",sid:"+sid+",relationNo:"+relationNo+",fee:"+fee+"}");
              OrgProfit orgProfit=new OrgProfit();
              orgProfit.setOrgId(new Long(orgId));
              orgProfit.setSid(new Long(sid));
              orgProfit.setFee(new BigDecimal(fee));
              orgProfit.setRelationNo(relationNo);
              resultJson=orgLogicService.insertOrgLogic(orgProfit,uuid);
              logger.info("uuid->"+uuid+",method=add,result="+resultJson);
          }catch (Exception e){
              e.printStackTrace();
              resultJson= ResultUtil.sendJson("fail","系统异常",null,null);
              logger.error("uuid->"+uuid+",method=add,params={orgId:"+orgId+",sid:"+sid+",relationNo:"+relationNo+",fee:"+fee+"}",e);
          }

            ResponseUtil.responseJson(response, resultJson);
    }


    }
