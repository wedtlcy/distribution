package com.xdream.distribution.controller;

import com.xdream.distribution.utils.ResultUtil;
import com.xdream.kernel.util.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@Controller
@RequestMapping(value="/user")
public class UserController {


    @RequestMapping(value="/getTranslsData")
    public void getTranslsData(HttpServletRequest request, HttpServletResponse response)throws Exception{
        String userId=request.getParameter("userId");//用户Id
        String dataType=request.getParameter("dataType");//类型，日统计还是月统计
        String transDate=request.getParameter("transDate");//交易日期
        String resultJson=null;
        if(StringUtils.isBlank(userId)){
            resultJson= ResultUtil.sendJson("fail","代理商id不能为空",null,null);
            ResponseUtil.responseJson(response, resultJson);
            return;
        }
        if(StringUtils.isBlank(dataType)){
            resultJson= ResultUtil.sendJson("fail","类型不能为空",null,null);
            ResponseUtil.responseJson(response, resultJson);
            return;
        }
        if(StringUtils.isBlank(transDate)){
            resultJson= ResultUtil.sendJson("fail","统计日期不能为空",null,null);
            ResponseUtil.responseJson(response, resultJson);
            return;
        }

    }

}
