package com.xdream.distribution.controller;

import com.xdream.distribution.service.ICalculFeeService;
import com.xdream.distribution.utils.ResultUtil;
import com.xdream.kernel.util.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;

@SuppressWarnings("serial")
@Controller
@RequestMapping(value="/calculate")
public class CalculateController {
    private static Logger logger = Logger.getLogger(CalculateController.class);

    @Resource(name= "calculFeeService")
    private ICalculFeeService calculFeeService;
    /**
     * 计算用户分润
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value="/profit")
    public void profit(HttpServletRequest request, HttpServletResponse response) throws Exception{
        String fee=request.getParameter("fee");//总手续费
        String userId = request.getParameter("userId");//用户id

        String resultJson=null;
        if(StringUtils.isBlank(fee)){
            resultJson= ResultUtil.sendJson("fail","总手续费不能为空",null,null);
            ResponseUtil.responseJson(response, resultJson);
            return;
        }
        if(StringUtils.isBlank(userId)){
            resultJson= ResultUtil.sendJson("fail","用户id不能为空",null,null);
            ResponseUtil.responseJson(response, resultJson);
            return;
        }
        try {
        resultJson=calculFeeService.calculProfit(new BigDecimal(fee),new Long(userId));
        ResponseUtil.responseJson(response, resultJson);
        }catch (Exception e){
            e.printStackTrace();
            resultJson= ResultUtil.sendJson("fail","系统异常",null,null);
            logger.error("method=profit,params={userId:"+userId+"fee:"+fee+"}",e);
        }
    }

}
