package com.xdream.distribution.utils;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

public class ResultUtil {

    public  static String sendJson(String code,String message,String data1,Object data2) throws Exception {

        Map<Object,Object> sendMap=new HashMap<Object, Object>();
        sendMap.put("code",code);
        sendMap.put("message",message);
        sendMap.put(data1,data2);
        String resultJson= JSON.toJSONString(sendMap);
        return  resultJson;
    }


}
