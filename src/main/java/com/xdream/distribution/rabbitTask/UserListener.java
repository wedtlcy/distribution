package com.xdream.distribution.rabbitTask;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.Channel;
import com.xdream.distribution.entity.UserGrade;
import com.xdream.distribution.entity.UserProfit;
import com.xdream.distribution.enums.IsDefault;
import com.xdream.distribution.enums.ProfitType;
import com.xdream.distribution.enums.State;
import com.xdream.distribution.enums.SystemParam;
import com.xdream.distribution.service.IUserGradeService;
import com.xdream.distribution.service.IUserProfitService;
import com.xdream.kernel.util.StringUtil;
import org.apache.log4j.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class UserListener implements ChannelAwareMessageListener {

    private static Logger logger = Logger.getLogger(UserListener.class);

    @Resource(name = "userProfitService")
    private IUserProfitService userProfitService;

    @Resource(name="userGradeService")
    private IUserGradeService userGradeService;
    //新增用户
    public void onMessage(Message message, Channel channel) throws Exception {
        String userMessageJson=new String(message.getBody(),"utf-8");
        String uuid= UUID.randomUUID().toString().replace("-", "");
        try {
            logger.info("uuid->"+uuid+",class=UserListener,params="+userMessageJson);
            Map userMap= JSON.parseObject(userMessageJson, HashMap.class);
            Integer userId2=(Integer) userMap.get("userId");//用户Id
            Integer  usid=(Integer)userMap.get("usid");//上级用户id
            String  urelationNo=(String)userMap.get("urelationNo");//用户关联
            Long userId=Long.valueOf(userId2);
            //获取用户费率
            UserProfit userProfit=userProfitService.findByUserId(userId, State.NORMAL.stateCode);
            if(userProfit!=null){
                logger.debug("uuid->"+uuid+",class=UserListener,params="+userMessageJson+",message={用户已添加,费率已存在}");
                channel.basicNack(message.getMessageProperties().getDeliveryTag(), false,false);
                return;
            }
            Long userGradeId = null;
            BigDecimal rate = null;
            //按利差分润需要获取费率填入用户
            if(SystemParam.SHAREPROFITTYPE.paramName.equals(ProfitType.DIFFERE)){
                //获取默认用户费率
                UserGrade userGrade=userGradeService.findByIsDefault(IsDefault.T.defaultCode,State.NORMAL.stateCode);
                if(userGrade == null){
                    logger.debug("uuid->"+uuid+",class=UserListener,params="+userMessageJson+",message={默认用户费率查询失败}");
                    channel.basicNack(message.getMessageProperties().getDeliveryTag(), false,false);
                    return;
                }
                userGradeId=userGrade.getId();
                rate=userGrade.getUserRate();
            }

            String createDate = StringUtil.getCurrentDateTime("yyyy-MM-dd");
            String createTime = StringUtil.getCurrentDateTime("HH:mm:ss");
            UserProfit addUserProfit = new UserProfit();
            addUserProfit.setFee(rate);
            addUserProfit.setState(State.NORMAL.stateCode);
            addUserProfit.setUrelationNo(urelationNo);
            addUserProfit.setUserGradeId(userGradeId);
            addUserProfit.setUserId(userId);
            addUserProfit.setUsid(new Long(usid));
            addUserProfit.setCreateDate(createDate);
            addUserProfit.setCreateTime(createTime);
            //新增用户费率
            Long id=userProfitService.insertUserProfit(addUserProfit);
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false,false);
        }catch (Exception e){
            e.printStackTrace();
            logger.error("uuid->"+uuid+",class=UserListener,params="+userMessageJson,e);
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false,false);
        }


    }
}
