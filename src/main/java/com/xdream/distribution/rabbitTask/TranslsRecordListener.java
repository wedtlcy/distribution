package com.xdream.distribution.rabbitTask;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.Channel;
import com.xdream.distribution.entity.TranslsRecord;
import com.xdream.distribution.enums.ProfitType;
import com.xdream.distribution.enums.Settle;
import com.xdream.distribution.enums.SystemParam;
import com.xdream.distribution.service.IProfitFactory;
import com.xdream.distribution.service.IPurchaseProfitService;
import com.xdream.distribution.service.ITranslsRecordService;
import com.xdream.kernel.util.StringUtil;
import org.apache.log4j.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class TranslsRecordListener implements ChannelAwareMessageListener {
    @Resource(name="translsRecordService")
    private ITranslsRecordService translsRecordService;

    @Resource(name ="profitFactory")
    private IProfitFactory profitFactory;

    private static Logger logger = Logger.getLogger(TranslsRecordListener.class);
    //流水记录
    public void onMessage(Message message, Channel channel) throws Exception {
        String translsRecordJson=new String(message.getBody(),"utf-8");
        String uuid= UUID.randomUUID().toString().replace("-", "");
        try{
            logger.info("uuid->"+uuid+",class=TranslsRecordListener,params="+translsRecordJson);
            Map translsRecordMap= JSON.parseObject(translsRecordJson, HashMap.class);
            String transDate=(String)translsRecordMap.get("transDate");//交易日期
            String transTime=(String)translsRecordMap.get("transTime");//交易时间
            String orderNo=(String) translsRecordMap.get("orderNo");//订单号
            String urelationNo=(String) translsRecordMap.get("urelationNo");//用户关系
            Integer amount=(Integer) translsRecordMap.get("amount");//交易金额
            Integer userId=(Integer) translsRecordMap.get("userId");//用户id
            Integer usid=(Integer) translsRecordMap.get("usid");//上级用户id
            Integer translsId=(Integer) translsRecordMap.get("translsId");//流水id
            Integer fee=(Integer)translsRecordMap.get("fee");//手续费
            Integer orgId=(Integer) translsRecordMap.get("orgId");//代理商id
            Integer sid=(Integer) translsRecordMap.get("sid");//上级代理商id
            String relationNo=(String) translsRecordMap.get("relationNo");//机构关联
            String createDate = StringUtil.getCurrentDateTime("yyyy-MM-dd");
            String createTime = StringUtil.getCurrentDateTime("HH:mm:ss");
            TranslsRecord translsRecord =new TranslsRecord();
            translsRecord.setCreateDate(createDate);
            translsRecord.setCreateTime(createTime);
            translsRecord.setAmount(new BigDecimal(amount));
            translsRecord.setIsSettle(Settle.NOIN.settleCode);
            translsRecord.setOrderNo(orderNo);
            translsRecord.setTransDate(transDate);
            translsRecord.setTransTime(transTime);
            translsRecord.setUrelationNo(urelationNo);
            translsRecord.setUserId(Long.valueOf(userId));
            translsRecord.setUsid(Long.valueOf(usid));
            translsRecord.setTranslsId(Long.valueOf(translsId));
            translsRecord.setFee(new BigDecimal(fee));
            translsRecord.setOrgId(new Long(orgId));
            translsRecord.setSid(new Long(sid));
            translsRecord.setRelationNo(relationNo);
            Long translsRecordId=translsRecordService.insertTranslsRecord(translsRecord);
            translsRecord.setId(translsRecordId);
            //计算分润
            profitFactory.profit(translsRecord, SystemParam.SHAREPROFITTYPE.paramName);
            //提交mq进行确认，不进行重新获取
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false,false);
        }catch (Exception e){
            e.printStackTrace();
            logger.error("uuid->"+uuid+",class=TranslsRecordListener,params="+translsRecordJson,e);
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false,false);
        }
    }

}
