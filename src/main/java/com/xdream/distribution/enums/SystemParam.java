package com.xdream.distribution.enums;

import java.math.BigDecimal;

public enum SystemParam {

    UserRate("20","直属上级固定费率"),
    SHAREPROFITTYPE(ProfitType.PERCENT.type,"系统分润类型");
    public String paramName;
    public String remark;

    SystemParam(String paramName,String remark){
        this.paramName=paramName;
        this.remark=remark;
    }

}
