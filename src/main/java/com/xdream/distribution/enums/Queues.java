package com.xdream.distribution.enums;

public enum Queues {
    DS_USERPROFIT("ds_userProfit","用户分润传输队列"),
    DS_ORGPROFIT("ds_orgProfit","代理商分润传输队列");
    public String queuesCode;
    public String queuesName;
    Queues(String queuesCode, String queuesName) {
        this.queuesCode = queuesCode;
        this.queuesName = queuesName;
    }
    public  Queues getByType(String queuesCode){
        for (Queues queues : values()) {
            if(queues.queuesCode.equals(queuesCode)){
                return queues;
            }
        }
        return null;
    }
}
