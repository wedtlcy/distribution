package com.xdream.distribution.enums;

public enum Settle {
    NOIN("F","未结算"),IN("T","已结算");
    public String settleCode;
    public String message;
    Settle(String settleCode, String message) {
        this.settleCode = settleCode;
        this.message = message;
    }
    public  Settle getByType(String settleCode){
        for (Settle settle : values()) {
            if(settle.settleCode.equals(settleCode)){
                return settle;
            }
        }
        return null;
    }
}
