package com.xdream.distribution.enums;

public enum IsDefault {
    T("T","默认"),F("F","正常");
    public String defaultCode;
    public String defaultMessage;
    IsDefault(String defaultCode, String defaultMessage) {
        this.defaultCode = defaultCode;
        this.defaultMessage = defaultMessage;
    }
    public  IsDefault getByType(String defaultCode){
        for (IsDefault isDefault : values()) {
            if(isDefault.defaultCode.equals(defaultCode)){
                return isDefault;
            }
        }
        return null;
    }
}
