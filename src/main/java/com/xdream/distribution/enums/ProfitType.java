package com.xdream.distribution.enums;

public enum ProfitType {
    DIFFERE("differe","按利差分润"),
    PERCENT("percent","直接百分比");
    public String type;
    public String  remark;
    ProfitType(String type, String remark) {
        this.type = type;
        this.remark = remark;
    }
}
