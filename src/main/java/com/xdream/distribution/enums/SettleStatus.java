package com.xdream.distribution.enums;

public enum SettleStatus {

    NOIN("F","未结算"),IN("T","已结算");
    public String settleCode;
    public String message;
    SettleStatus(String settleCode, String message) {
        this.settleCode = settleCode;
        this.message = message;
    }
    public  SettleStatus getByType(String settleCode){
        for (SettleStatus settleStatus : values()) {
            if(settleStatus.settleCode.equals(settleCode)){
                return settleStatus;
            }
        }
        return null;
    }
}
