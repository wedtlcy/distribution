package com.xdream.distribution.enums;

public enum State {
    NORMAL("normal","正常"),FREEZE("freeze","冻结");
    public String stateCode;
    public String stateMessage;
    State(String stateCode, String stateMessage) {
        this.stateCode = stateCode;
        this.stateMessage = stateMessage;
    }
    public  State getByType(String stateCode){
        for (State state : values()) {
            if(state.stateCode.equals(stateCode)){
                return state;
            }
        }
        return null;
    }
}
