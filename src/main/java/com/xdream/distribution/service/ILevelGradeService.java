package com.xdream.distribution.service;
import com.xdream.distribution.entity.LevelGrade;

import java.util.List;

/**
    * tbl_level_grade service 接口<br/>
    * 2020-03-23 09:56:08 huangbx
    */ 
public interface ILevelGradeService {


   /**
    * 查找所有等级
    * @param state 状态
    * @return
    * @throws Exception
    */
   public List<LevelGrade> findAllLevel(String state) throws Exception;

}

