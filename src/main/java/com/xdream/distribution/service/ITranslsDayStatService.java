package com.xdream.distribution.service;
import com.xdream.distribution.entity.TranslsDayStat;

import java.util.List;

/**
    * tbl_transls_day_stat service 接口<br/>
    * 2020-03-13 12:11:07 huangbx
    */ 
public interface ITranslsDayStatService {

      /**
       * 添加日统计流水数据
       * @param translsDayStat 日统计
       * @return
       * @throws Exception
       */
   public  Long addTranslsDayStat(TranslsDayStat translsDayStat) throws Exception;

    /**
     * 获取日流水数据
     * @param settleDate 日期
     * @param minUserId
     * @param maxUserId
     * @return
     * @throws Exception
     */
   public  List<TranslsDayStat> findByUserId(String settleDate,Long minUserId,Long maxUserId) throws Exception;
}

