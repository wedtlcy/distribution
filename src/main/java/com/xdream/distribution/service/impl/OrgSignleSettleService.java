package com.xdream.distribution.service.impl;

import com.xdream.distribution.dao.IOrgSignleSettleDao;
import com.xdream.distribution.entity.OrgSignleSettle;
import com.xdream.distribution.service.IOrgSignleSettleService;
import com.xdream.kernel.sql.SQLConf;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("orgSignleSettleService")
public class OrgSignleSettleService implements IOrgSignleSettleService {
	@Resource(name = "orgSignleSettleDao") 
	private IOrgSignleSettleDao orgSignleSettleDao;
	/**
	 * 添加代理商结算
	 * @param orgSignleSettle
	 * @return
	 * @throws Exception
	 */
	public Long addOrgSingleSettle(OrgSignleSettle orgSignleSettle) throws Exception {
		String sql= SQLConf.getSql("orgSignleSettle","addOrgSingleSettle");
		return orgSignleSettleDao.insert(sql,orgSignleSettle);
	}

	/**
	 * 统计用户单日总分润金额
	 * @param orgId  代理商id
	 * @param settleDate 结算日期
	 * @return
	 * @throws Exception
	 */
	public OrgSignleSettle findByOrgId(Long orgId, String settleDate) throws Exception {
		String sql= SQLConf.getSql("orgSignleSettle","findByOrgId");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("orgId", orgId);
		map.put("settleDate", settleDate);
		List<OrgSignleSettle> orgSingleSettleList = orgSignleSettleDao.find(sql, map);
		if (orgSingleSettleList!=null && !orgSingleSettleList.isEmpty() && orgSingleSettleList.size()>0){
			return orgSingleSettleList.get(0);
		}
		return null;
	}
}

