package com.xdream.distribution.service.impl;
import javax.annotation.Resource;

import com.xdream.kernel.sql.SQLConf;
import org.springframework.stereotype.Service;
import com.xdream.distribution.dao.ITranslsRecordDao;
import com.xdream.distribution.service.ITranslsRecordService;
import com.xdream.distribution.entity.TranslsRecord;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("translsRecordService")
public class TranslsRecordService implements ITranslsRecordService {
	@Resource(name = "translsRecordDao") 
	private ITranslsRecordDao translsRecordDao;

	/**
	 * 插入流水记录表
	 * @param translsRecord 流水记录
	 * @return
	 * @throws Exception
	 */
	public Long insertTranslsRecord(TranslsRecord translsRecord) throws Exception {
		String sql= SQLConf.getSql("translsRecord","insertTranslsRecord");
		return translsRecordDao.insert(sql,translsRecord);
	}
	/**
	 * 更改流水结算
	 * @param translsRecordId
	 * @param isSettle
	 * @return
	 * @throws Exception
	 */
	public int updateIsSettle(Long translsRecordId, String isSettle) throws Exception {
		String sql=SQLConf.getSql("translsRecord","updateIsSettle");
		Map<String,Object> translsRecordMap=new HashMap<String, Object>();
		translsRecordMap.put("id",translsRecordId);
		translsRecordMap.put("isSettle",isSettle);
		return translsRecordDao.update(sql,translsRecordMap);
	}
	/**
	 * 通过用id统计流水
	 * @param settleDate 结算日期
	 * @param minUserId  最小用户id
	 * @param maxUserId  最高用户id
	 * @return
	 * @throws Exception
	 */
	public List<TranslsRecord> sumAmount(String settleDate, Long minUserId, Long maxUserId) throws Exception {
		String sql=SQLConf.getSql("translsRecord","sumAmount");
		Map<String,Object> translsRecordMap=new HashMap<String, Object>();
		translsRecordMap.put("settleDate",settleDate);
		translsRecordMap.put("minUserId",minUserId);
		translsRecordMap.put("maxUserId",maxUserId);
		List<TranslsRecord> translsRecordList=translsRecordDao.find(sql,translsRecordMap);
		if (translsRecordList!=null && !translsRecordList.isEmpty() && translsRecordList.size()>0) {
		   return  translsRecordList;
		}
		return null;
	}

	/**
	 * 通过代理商id统计流水
	 * @param settleDate  结算日期
	 * @param minOrgId
	 * @param maxOrgId
	 * @return
	 * @throws Exception
	 */
	public List<TranslsRecord> sumOrgAmount(String settleDate, Long minOrgId, Long maxOrgId) throws Exception {
		String sql=SQLConf.getSql("translsRecord","sumOrgAmount");
		Map<String,Object> translsRecordMap=new HashMap<String, Object>();
		translsRecordMap.put("settleDate",settleDate);
		translsRecordMap.put("minOrgId",minOrgId);
		translsRecordMap.put("maxOrgId",maxOrgId);
		List<TranslsRecord> translsRecordList=translsRecordDao.find(sql,translsRecordMap);
		if (translsRecordList!=null && !translsRecordList.isEmpty() && translsRecordList.size()>0) {
			return  translsRecordList;
		}
		return null;
	}
}

