package com.xdream.distribution.service.impl;
import javax.annotation.Resource;

import com.xdream.kernel.sql.SQLConf;
import org.springframework.stereotype.Service;
import com.xdream.distribution.dao.IUserGradeDao;
import com.xdream.distribution.service.IUserGradeService;
import com.xdream.distribution.entity.UserGrade;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("userGradeService")
public class UserGradeService implements IUserGradeService {
	@Resource(name = "userGradeDao") 
	private IUserGradeDao userGradeDao;

	/**
	 * 查找默认等级
	 * @param isDefault 默认等级
	 * @param state 状态
	 * @return
	 * @throws Exception
	 */
	public UserGrade findByIsDefault(String isDefault, String state) throws Exception {
		String sql= SQLConf.getSql("userGrade","findByIsDefault");
		Map<String,Object> userGradeMap=new HashMap<String, Object>();
		userGradeMap.put("isDefault",isDefault);
		userGradeMap.put("state",state);
		List<UserGrade> userGradeList=userGradeDao.find(sql,userGradeMap);
		if (userGradeList!=null && !userGradeList.isEmpty() && userGradeList.size()>0){
			return  userGradeList.get(0);
		}
		return null;
	}
}

