package com.xdream.distribution.service.impl;
import javax.annotation.Resource;

import com.xdream.kernel.sql.SQLConf;
import org.springframework.stereotype.Service;
import com.xdream.distribution.dao.IUserMonthSettleDao;
import com.xdream.distribution.service.IUserMonthSettleService;
import com.xdream.distribution.entity.UserMonthSettle;
@Service("userMonthSettleService")
public class UserMonthSettleService implements IUserMonthSettleService {
	@Resource(name = "userMonthSettleDao") 
	private IUserMonthSettleDao userMonthSettleDao;
	/**
	 * 添加用户结算数据
	 * @param userMonthSettle
	 * @return
	 * @throws Exception
	 */
	public Long addUserMonthSettle(UserMonthSettle userMonthSettle) throws Exception {

		String sql= SQLConf.getSql("userMonthSettle","addUserMonthSettle");

		return userMonthSettleDao.insert(sql,userMonthSettle);
	}
}

