package com.xdream.distribution.service.impl;
import javax.annotation.Resource;

import com.xdream.kernel.sql.SQLConf;
import org.springframework.stereotype.Service;
import com.xdream.distribution.dao.ITranslsMonthStatDao;
import com.xdream.distribution.service.ITranslsMonthStatService;
import com.xdream.distribution.entity.TranslsMonthStat;
@Service("translsMonthStatService")
public class TranslsMonthStatService implements ITranslsMonthStatService {
	@Resource(name = "translsMonthStatDao") 
	private ITranslsMonthStatDao translsMonthStatDao;

	/**
	 * 添加用户流水月统计
	 * @param translsMonthStat
	 * @return
	 * @throws Exception
	 */
	public Long addTranslsMonthStat(TranslsMonthStat translsMonthStat) throws Exception {
		String sql = SQLConf.getSql("translsMonthStat","addTranslsMonthStat");
		return translsMonthStatDao.insert(sql,translsMonthStat);
	}
}

