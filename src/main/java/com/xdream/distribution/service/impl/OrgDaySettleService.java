package com.xdream.distribution.service.impl;
import javax.annotation.Resource;

import com.xdream.kernel.sql.SQLConf;
import org.springframework.stereotype.Service;
import com.xdream.distribution.dao.IOrgDaySettleDao;
import com.xdream.distribution.service.IOrgDaySettleService;
import com.xdream.distribution.entity.OrgDaySettle;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("orgDaySettleService")
public class OrgDaySettleService implements IOrgDaySettleService {
	@Resource(name = "orgDaySettleDao") 
	private IOrgDaySettleDao orgDaySettleDao;
	/**
	 * 代理商日结算
	 * @param orgDaySettle
	 * @return
	 * @throws Exception
	 */
	public Long addOrgDaySettle(OrgDaySettle orgDaySettle) throws Exception {
		String sql= SQLConf.getSql("orgDaySettle","addOrgDaySettle");
		return orgDaySettleDao.insert(sql,orgDaySettle);
	}

	/**
	 * 获取代理商月统计
	 * @param settleDate 统计日期
	 * @param minOrgId
	 * @param maxOrgId
	 * @return
	 * @throws Exception
	 */
	public List<OrgDaySettle> findByDateAndOrgId(String settleDate, Long minOrgId, Long maxOrgId) throws Exception {

		String sql = SQLConf.getSql("orgDaySettle","findByDateAndOrgId");
		Map<String,Object> orgDaySettleMap=new HashMap<String, Object>();
		orgDaySettleMap.put("settleDate",settleDate+"%");
		orgDaySettleMap.put("minOrgId",minOrgId);
		orgDaySettleMap.put("maxOrgId",maxOrgId);
		List<OrgDaySettle> orgDaySettleList=orgDaySettleDao.find(sql,orgDaySettleMap);
		if (orgDaySettleList!=null && !orgDaySettleList.isEmpty() && orgDaySettleList.size()>0) {
			return  orgDaySettleList;
		}
		return null;
	}

	/**
	 * 获取代理商日统计数据
	 * @param settleDate
	 * @param minOrgId
	 * @param maxOrgId
	 * @return
	 * @throws Exception
	 */
	public List<OrgDaySettle> findByDateSettle(String settleDate, Long minOrgId, Long maxOrgId) throws Exception {
		String sql = SQLConf.getSql("orgDaySettle","findDaySettle");
		Map<String,Object> orgDaySettleMap=new HashMap<String, Object>();
		orgDaySettleMap.put("settleDate",settleDate);
		orgDaySettleMap.put("minOrgId",minOrgId);
		orgDaySettleMap.put("maxOrgId",maxOrgId);
		List<OrgDaySettle> orgDaySettleList=orgDaySettleDao.find(sql,orgDaySettleMap);
		if (orgDaySettleList!=null && !orgDaySettleList.isEmpty() && orgDaySettleList.size()>0) {
			return  orgDaySettleList;
		}
		return null;
	}
	/**
	 * 更改结算状态
	 * @param settleDate 结算日期
	 * @param minOrgId
	 * @param maxOrgId
	 * @param settleStatus 结算状态
	 * @return
	 * @throws Exception
	 */
	public int updateOrgDaySettleStatus(String settleDate, Long minOrgId, Long maxOrgId, String settleStatus) throws Exception {
		String sql = SQLConf.getSql("orgDaySettle","updateOrgDaySettleStatus");
		Map<String,Object> translsDayStatMap=new HashMap<String, Object>();
		translsDayStatMap.put("settleDate",settleDate);
		translsDayStatMap.put("minOrgId",minOrgId);
		translsDayStatMap.put("maxOrgId",maxOrgId);
		translsDayStatMap.put("settleStatus",settleStatus);
		return orgDaySettleDao.update(sql,translsDayStatMap) ;
	}
}

