package com.xdream.distribution.service.impl;

import com.xdream.distribution.dao.ILevelGradeDao;
import com.xdream.distribution.entity.LevelGrade;
import com.xdream.distribution.service.ILevelGradeService;
import com.xdream.kernel.sql.SQLConf;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("levelGradeService")
public class LevelGradeService implements ILevelGradeService {
	@Resource(name = "levelGradeDao") 
	private ILevelGradeDao levelGradeDao;

	/**
	 * 查找所有等级
	 * @param state 状态
	 * @return
	 * @throws Exception
	 */
	public List<LevelGrade> findAllLevel(String state) throws Exception {
		String sql= SQLConf.getSql("levelGrade","findAllLevel");
		Map<String,Object> levelGradeMap=new HashMap<String, Object>();
		levelGradeMap.put("state",state);
		List<LevelGrade> levelGradeList=levelGradeDao.find(sql,levelGradeMap);
		if (levelGradeList!=null && !levelGradeList.isEmpty() && levelGradeList.size()>0){
			return  levelGradeList;
		}
		return null;
	}
}

