package com.xdream.distribution.service.impl;
import javax.annotation.Resource;

import com.xdream.kernel.sql.SQLConf;
import org.springframework.stereotype.Service;
import com.xdream.distribution.dao.IUserSingleSettleDao;
import com.xdream.distribution.service.IUserSingleSettleService;
import com.xdream.distribution.entity.UserSingleSettle;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("userSingleSettleService")
public class UserSingleSettleService implements IUserSingleSettleService {
	@Resource(name = "userSingleSettleDao") 
	private IUserSingleSettleDao userSingleSettleDao;

	/**
	 * 插入用户结算
	 * @param userSingleSettle
	 * @return
	 * @throws Exception
	 */
	public Long addUserSingleSettle(UserSingleSettle userSingleSettle) throws Exception {
		String sql= SQLConf.getSql("userSingleSettle","addUserSingleSettle");
		return userSingleSettleDao.insert(sql,userSingleSettle);
	}
	/**
	 * 统计用户单日总分润金额
	 * @param userId  用户id
	 * @param settleDate 结算日期
	 * @return
	 * @throws Exception
	 */
	public UserSingleSettle findByUserId(Long userId, String settleDate) throws Exception {
		String sql= SQLConf.getSql("userSingleSettle","findByUserId");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("userId", userId);
		map.put("settleDate", settleDate);
		List<UserSingleSettle> userSingleSettleList = userSingleSettleDao.find(sql, map);
		if (userSingleSettleList!=null && !userSingleSettleList.isEmpty() && userSingleSettleList.size()>0){
			return userSingleSettleList.get(0);
		}
		return null;
	}
}

