package com.xdream.distribution.service.impl;
import javax.annotation.Resource;

import com.xdream.kernel.sql.SQLConf;
import org.springframework.stereotype.Service;
import com.xdream.distribution.dao.IOrgTranslsMonthDao;
import com.xdream.distribution.service.IOrgTranslsMonthService;
import com.xdream.distribution.entity.OrgTranslsMonth;
@Service("orgTranslsMonthService")
public class OrgTranslsMonthService implements IOrgTranslsMonthService {
	@Resource(name = "orgTranslsMonthDao") 
	private IOrgTranslsMonthDao orgTranslsMonthDao;
	/**
	 * 新增机构月统计
	 * @param orgTranslsMonth 代理商月统计
	 * @return
	 * @throws Exception
	 */
	public Long addOrgTranslsMonth(OrgTranslsMonth orgTranslsMonth) throws Exception {
		String sql= SQLConf.getSql("orgTranslsMonth","addOrgTranslsMonth");

		return orgTranslsMonthDao.insert(sql,orgTranslsMonth);
	}
}

