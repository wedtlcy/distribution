package com.xdream.distribution.service.impl;
import javax.annotation.Resource;

import com.xdream.kernel.sql.SQLConf;
import org.springframework.stereotype.Service;
import com.xdream.distribution.dao.IUserProfitDao;
import com.xdream.distribution.service.IUserProfitService;
import com.xdream.distribution.entity.UserProfit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("userProfitService")
public class UserProfitService implements IUserProfitService {
	@Resource(name = "userProfitDao") 
	private IUserProfitDao userProfitDao;

	/**
	 * 获取用户费率
	 * @param userId 用户Id
	 * @param state  状态
	 * @return
	 * @throws Exception
	 */
	public UserProfit findByUserId(Long userId, String state) throws Exception {
		String sql= SQLConf.getSql("userProfit","findByUserId");
		Map<String,Object> userProfitMap=new HashMap<String, Object>();
		userProfitMap.put("userId",userId);
		userProfitMap.put("state",state);
		List<UserProfit> userProfitList=userProfitDao.find(sql,userProfitMap);
		if (userProfitList!=null && !userProfitList.isEmpty() && userProfitList.size()>0){
			return  userProfitList.get(0);
		}
		return null;
	}
	/**
	 * 插入用户分润
	 * @param userProfit 用户分润
	 * @return
	 * @throws Exception
	 */
	public Long insertUserProfit(UserProfit userProfit) throws Exception {
		String sql=SQLConf.getSql("userProfit","insertUserProfit");

		return userProfitDao.insert(sql,userProfit);
	}
	/**
	 * 统计用户个数
	 * @param
	 * @return
	 * @throws Exception
	 */
	public Long countUser() throws Exception {
		String sql=SQLConf.getSql("userProfit", "countUser");
		Map<String,Object> map = new HashMap<String,Object>();
		Long count=userProfitDao.count2Long(sql, map);
		return count;
	}

	/**
	 * 分页获取用户
	 * @param beginNums
	 * @param total
	 * @return
	 */
	public List<UserProfit> find(int beginNums, int total) {
		String sql=SQLConf.getSql("userProfit", "find");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("beginNums", beginNums);
		map.put("total", total);
		List<UserProfit> userProfitList = userProfitDao.find(sql, map);
		if (userProfitList!=null && !userProfitList.isEmpty() && userProfitList.size()>0){
			return userProfitList;
		}
		return null;
	}
}

