package com.xdream.distribution.service.impl;
import com.xdream.distribution.entity.*;
import com.xdream.distribution.enums.Settle;
import com.xdream.distribution.enums.SettleStatus;
import com.xdream.distribution.enums.State;
import com.xdream.distribution.service.*;
import com.xdream.kernel.util.StringUtil;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service("percentPurchaseService")
public class PercentPurchaseService implements IPurchaseProfitService {

    @Resource(name="userProfitService")
    private IUserProfitService userProfitService;

    @Resource(name="userSingleSettleService")
    private IUserSingleSettleService userSingleSettleService;

    @Resource(name="orgSignleSettleService")
    private IOrgSignleSettleService orgSignleSettleService;

    @Resource(name="translsRecordService")
    private ITranslsRecordService translsRecordService;

    @Resource(name="orgProfitService")
    private IOrgProfitService orgProfitService;

    @Resource(name="levelGradeService")
    private  ILevelGradeService levelGradeService;


    /**
     * 按用户等级百分比计算
     * 1.先获取分润的等级，等级从接近用户下单的那个算一级往上数
     * 2.通过总等级，依次对每一级用户进行分润
     * 3.将每一级别用户分润添加用户分润表
     * 4.计算平台分润，并添加平台分润表
     * @throws Exception
     */
    public void userProfit(TranslsRecord translsRecord) throws Exception {

        BigDecimal fee = translsRecord.getFee();//总手续费
        BigDecimal sumProfit=BigDecimal.ZERO;//总分润金额
        //获取用户费率
        UserProfit userProfit=null;
        //用户所有等级
        List<LevelGrade> levelGrades = levelGradeService.findAllLevel(State.NORMAL.stateCode);
        //更改为已结算
        translsRecordService.updateIsSettle(translsRecord.getId(), Settle.IN.settleCode);
        if(levelGrades != null){
            Long userId=translsRecord.getUserId();
            for (LevelGrade levelGrade:levelGrades) {
                //顶级用户
                if(userId == 0){
                    continue;
                }
                //获取用户等级
                userProfit=userProfitService.findByUserId(userId, State.NORMAL.stateCode);
                //计算利润
                BigDecimal profit=fee.multiply(levelGrade.getLevelRate()).setScale(2, RoundingMode.DOWN);
                UserSingleSettle userSingleSettle=new UserSingleSettle();
                userSingleSettle.setSettleDate(StringUtil.getCurrentDateTime("yyyy-MM-dd"));
                userSingleSettle.setAmount(profit);
                userSingleSettle.setSettleStatus(SettleStatus.NOIN.settleCode);
                userSingleSettle.setUrelationNo(userProfit.getUrelationNo());
                userSingleSettle.setTranslsRecordId(translsRecord.getId());
                userSingleSettle.setUserId(userProfit.getUserId());
                userSingleSettleService.addUserSingleSettle(userSingleSettle);
                sumProfit=sumProfit.add(profit);
                userId=userProfit.getUsid();
            }
        }
        //运营商结算
        BigDecimal businessAmount=translsRecord.getFee().subtract(sumProfit);
        OrgSignleSettle orgSignleSettle=new OrgSignleSettle();
        orgSignleSettle.setAmount(businessAmount);
        orgSignleSettle.setOrgId(new Long(1));
        orgSignleSettle.setRelationNo("1.");
        orgSignleSettle.setSettleDate(StringUtil.getCurrentDateTime("yyyy-MM-dd"));
        orgSignleSettle.setSettleStatus(SettleStatus.NOIN.settleCode);
        orgSignleSettle.setTranslsRecordId(translsRecord.getId());
        orgSignleSettleService.addOrgSingleSettle(orgSignleSettle);


    }
}
