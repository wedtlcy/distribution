package com.xdream.distribution.service.impl;

import com.xdream.distribution.dao.IOrgProfitDao;
import com.xdream.distribution.entity.OrgProfit;
import com.xdream.distribution.entity.UserProfit;
import com.xdream.distribution.service.IOrgProfitService;
import com.xdream.kernel.sql.SQLConf;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("orgProfitService")
public class OrgProfitService implements IOrgProfitService {
	@Resource(name = "orgProfitDao") 
	private IOrgProfitDao orgProfitDao;

	/**
	 * 通过代理商id获取代理商费率
	 * @param orgId  代理id
	 * @param states  状态
	 * @return
	 * @throws Exception
	 */
	public OrgProfit findByOrgId(Long orgId, String states) throws Exception {
		String sql= SQLConf.getSql("orgProfit","findByOrgId");
		Map<String,Object> orgProfitMap=new HashMap<String, Object>();
		orgProfitMap.put("orgId",orgId);
		orgProfitMap.put("state",states);
		List<OrgProfit> orgProfitList=orgProfitDao.find(sql,orgProfitMap);
		if (orgProfitList!=null && !orgProfitList.isEmpty() && orgProfitList.size()>0){
			return  orgProfitList.get(0);
		}
		return null;
	}

	/**
	 * 插入代理商费率表
	 * @param orgProfit
	 * @return
	 * @throws Exception
	 */
	public Long insertOrgProfit(OrgProfit orgProfit) throws Exception {
		String sql= SQLConf.getSql("orgProfit","insertOrgProfit");
		return orgProfitDao.insert(sql,orgProfit);
	}

	/**
	 * 统计代理商个数
	 * @param
	 * @return
	 * @throws Exception
	 */
	public Long countOrg() throws Exception {
		String sql=SQLConf.getSql("orgProfit", "countOrg");
		Map<String,Object> map = new HashMap<String,Object>();
		Long count=orgProfitDao.count2Long(sql, map);
		return count;
	}

	/**
	 * 分页获取代理商
	 * @param beginNums
	 * @param total
	 * @return
	 */
	public List<OrgProfit> find(int beginNums, int total) {
		String sql=SQLConf.getSql("orgProfit", "find");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("beginNums", beginNums);
		map.put("total", total);
		List<OrgProfit> orgProfitList = orgProfitDao.find(sql, map);
		if (orgProfitList!=null && !orgProfitList.isEmpty() && orgProfitList.size()>0){
			return orgProfitList;
		}
		return null;
	}
}

