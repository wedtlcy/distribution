package com.xdream.distribution.service.impl;
import javax.annotation.Resource;

import com.xdream.kernel.sql.SQLConf;
import org.springframework.stereotype.Service;
import com.xdream.distribution.dao.IOrgMonthSettleDao;
import com.xdream.distribution.service.IOrgMonthSettleService;
import com.xdream.distribution.entity.OrgMonthSettle;
@Service("orgMonthSettleService")
public class OrgMonthSettleService implements IOrgMonthSettleService {
	@Resource(name = "orgMonthSettleDao") 
	private IOrgMonthSettleDao orgMonthSettleDao;
	/**
	 * 添加代理商月分润
	 * @param orgMonthSettle
	 * @return
	 * @throws Exception
	 */
	public Long addOrgMonthSettle(OrgMonthSettle orgMonthSettle) throws Exception {
		String sql= SQLConf.getSql("orgMonthSettle","addOrgMonthSettle");
		return orgMonthSettleDao.insert(sql,orgMonthSettle);
	}
}

