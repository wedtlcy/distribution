package com.xdream.distribution.service.impl;

import com.xdream.distribution.entity.TranslsRecord;
import com.xdream.distribution.enums.ProfitType;
import com.xdream.distribution.service.IProfitFactory;
import com.xdream.distribution.service.IPurchaseProfitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("profitFactory")
public class ProfitFactory  implements IProfitFactory{
    @Autowired
    @Qualifier("purchaseProfitService")
   private IPurchaseProfitService purchaseProfitService;

    @Autowired
    @Qualifier("percentPurchaseService")
    private IPurchaseProfitService percentPurchaseService;
    /**
     * 分润工厂
     * @param translsRecord 流水记录
     * @param type  类型
     * @throws Exception
     */
    public void profit(TranslsRecord translsRecord, String type) throws Exception {
        if(ProfitType.PERCENT.type.equals(type)){
            percentPurchaseService.userProfit(translsRecord);
        }else if(ProfitType.DIFFERE.equals(type)){
            purchaseProfitService.userProfit(translsRecord);
        }

    }
}
