package com.xdream.distribution.service.impl;
import javax.annotation.Resource;

import com.xdream.distribution.entity.TranslsDayStat;
import com.xdream.kernel.sql.SQLConf;
import org.springframework.stereotype.Service;
import com.xdream.distribution.dao.IOrgTranslsDayDao;
import com.xdream.distribution.service.IOrgTranslsDayService;
import com.xdream.distribution.entity.OrgTranslsDay;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("orgTranslsDayService")
public class OrgTranslsDayService implements IOrgTranslsDayService {
	@Resource(name = "orgTranslsDayDao") 
	private IOrgTranslsDayDao orgTranslsDayDao;

	/**
	 * 添加代理商日统计
	 * @param orgTranslsDay
	 * @return
	 * @throws Exception
	 */
	public long addOrgTranslsDay(OrgTranslsDay orgTranslsDay) throws Exception {
		String sql= SQLConf.getSql("orgTranslsDay","addOrgTranslsDay");
		return orgTranslsDayDao.insert(sql,orgTranslsDay);
	}

	/**
	 * 获取代理商月统计
	 * @param settleDate 统计日期
	 * @param minOrgId
	 * @param maxOrgId
	 * @return
	 * @throws Exception
	 */
	public List<OrgTranslsDay> findByOrgId(String settleDate, Long minOrgId, Long maxOrgId) throws Exception {
		String sql = SQLConf.getSql("orgTranslsDay","findByOrgId");
		Map<String,Object> translsDayStatMap=new HashMap<String, Object>();
		translsDayStatMap.put("settleDate",settleDate+"%");
		translsDayStatMap.put("minOrgId",minOrgId);
		translsDayStatMap.put("maxOrgId",maxOrgId);
		List<OrgTranslsDay> orgTranslsDayList=orgTranslsDayDao.find(sql,translsDayStatMap);
		if (orgTranslsDayList!=null && !orgTranslsDayList.isEmpty() && orgTranslsDayList.size()>0) {
			return  orgTranslsDayList;
		}
		return null;
	}
}

