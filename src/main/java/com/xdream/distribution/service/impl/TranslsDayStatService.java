package com.xdream.distribution.service.impl;

import com.xdream.distribution.dao.ITranslsDayStatDao;
import com.xdream.distribution.entity.TranslsDayStat;
import com.xdream.distribution.service.ITranslsDayStatService;
import com.xdream.kernel.sql.SQLConf;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("translsDayStatService")
public class TranslsDayStatService implements ITranslsDayStatService {
	@Resource(name = "translsDayStatDao") 
	private ITranslsDayStatDao translsDayStatDao;

	/**
	 * 添加日统计流水数据
	 * @param translsDayStat 日统计
	 * @return
	 * @throws Exception
	 */
	public Long addTranslsDayStat(TranslsDayStat translsDayStat) throws Exception {
		String sql = SQLConf.getSql("translsDayStat","addTranslsDayStat");
		return translsDayStatDao.insert(sql,translsDayStat);
	}
	/**
	 * 获取日流水数据
	 * @param settleDate 日期
	 * @param minUserId
	 * @param maxUserId
	 * @return
	 * @throws Exception
	 */
	public List<TranslsDayStat> findByUserId(String settleDate, Long minUserId, Long maxUserId) throws Exception {
		String sql = SQLConf.getSql("translsDayStat","findByUserId");
		Map<String,Object> translsDayStatMap=new HashMap<String, Object>();
		translsDayStatMap.put("settleDate",settleDate+"%");
		translsDayStatMap.put("minUserId",minUserId);
		translsDayStatMap.put("maxUserId",maxUserId);
		List<TranslsDayStat> translsDayStatList=translsDayStatDao.find(sql,translsDayStatMap);
		if (translsDayStatList!=null && !translsDayStatList.isEmpty() && translsDayStatList.size()>0) {
			return  translsDayStatList;
		}
		return null;
	}
}

