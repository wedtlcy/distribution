package com.xdream.distribution.service.impl;
import javax.annotation.Resource;

import com.xdream.distribution.entity.TranslsDayStat;
import com.xdream.kernel.sql.SQLConf;
import org.springframework.stereotype.Service;
import com.xdream.distribution.dao.IUserDaySettleDao;
import com.xdream.distribution.service.IUserDaySettleService;
import com.xdream.distribution.entity.UserDaySettle;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("userDaySettleService")
public class UserDaySettleService implements IUserDaySettleService {
	@Resource(name = "userDaySettleDao") 
	private IUserDaySettleDao userDaySettleDao;
	/**
	 * 添加用户日统计
	 * @param userDaySettle
	 * @return
	 * @throws Exception
	 */
	public Long addUserDaySettle(UserDaySettle userDaySettle) throws Exception {
		String sql= SQLConf.getSql("userDaySettle","addUserDaySettle");
		return userDaySettleDao.insert(sql,userDaySettle);
	}

	/**
	 * 获取用户月统计数据
	 * @param settleDate
	 * @param minUserId
	 * @param maxUserId
	 * @return
	 * @throws Exception
	 */
	public List<UserDaySettle> findByDateAndUserId(String settleDate, Long minUserId, Long maxUserId) throws Exception {
		String sql = SQLConf.getSql("userDaySettle","findByDateAndUserId");
		Map<String,Object> translsDayStatMap=new HashMap<String, Object>();
		translsDayStatMap.put("settleDate",settleDate+"%");
		translsDayStatMap.put("minUserId",minUserId);
		translsDayStatMap.put("maxUserId",maxUserId);
		List<UserDaySettle> userDaySettleList=userDaySettleDao.find(sql,translsDayStatMap);
		if (userDaySettleList!=null && !userDaySettleList.isEmpty() && userDaySettleList.size()>0) {
			return  userDaySettleList;
		}
		return null;
	}

	/**
	 * 获取用户日统计数据
	 * @param settleDate
	 * @param minUserId
	 * @param maxUserId
	 * @return
	 * @throws Exception
	 */
	public List<UserDaySettle> findDaySettle(String settleDate, Long minUserId, Long maxUserId) throws Exception {
		String sql = SQLConf.getSql("userDaySettle","findDaySettle");
		Map<String,Object> translsDayStatMap=new HashMap<String, Object>();
		translsDayStatMap.put("settleDate",settleDate);
		translsDayStatMap.put("minUserId",minUserId);
		translsDayStatMap.put("maxUserId",maxUserId);
		List<UserDaySettle> userDaySettleList=userDaySettleDao.find(sql,translsDayStatMap);
		if (userDaySettleList!=null && !userDaySettleList.isEmpty() && userDaySettleList.size()>0) {
			return  userDaySettleList;
		}
		return null;
	}
	/**
	 * 更改结算状态
	 * @param settleDate 结算日期
	 * @param minUserId 用户id
	 * @param maxUserId
	 * @param settleStatus 结算状态
	 * @return
	 * @throws Exception
	 */
	public int updateSettleStatus(String settleDate, Long minUserId, Long maxUserId, String settleStatus) throws Exception {

		String sql = SQLConf.getSql("userDaySettle","updateSettleStatus");
		Map<String,Object> translsDayStatMap=new HashMap<String, Object>();
		translsDayStatMap.put("settleDate",settleDate);
		translsDayStatMap.put("minUserId",minUserId);
		translsDayStatMap.put("maxUserId",maxUserId);
		translsDayStatMap.put("settleStatus",settleStatus);
		return userDaySettleDao.update(sql,translsDayStatMap) ;
	}
}

