package com.xdream.distribution.service.impl;

import com.xdream.distribution.entity.*;
import com.xdream.distribution.enums.Settle;
import com.xdream.distribution.enums.SettleStatus;
import com.xdream.distribution.enums.State;
import com.xdream.distribution.enums.SystemParam;
import com.xdream.distribution.service.*;
import com.xdream.kernel.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;


@Service("purchaseProfitService")
public class PurchaseProfitService implements IPurchaseProfitService {
    @Resource(name="userProfitService")
    private IUserProfitService userProfitService;

    @Resource(name="userSingleSettleService")
    private IUserSingleSettleService userSingleSettleService;

    @Resource(name="orgSignleSettleService")
    private IOrgSignleSettleService orgSignleSettleService;

    @Resource(name="translsRecordService")
    private ITranslsRecordService translsRecordService;

    @Resource(name="orgProfitService")
    private IOrgProfitService orgProfitService;

    public void userProfit( TranslsRecord translsRecord ) throws Exception{
        //直属用户手续费
        BigDecimal dirtUserFee=translsRecord.getFee().multiply(new BigDecimal(SystemParam.UserRate.paramName).divide(new BigDecimal("100"))).setScale(2, RoundingMode.DOWN);

        //剩下参与分润的钱
        BigDecimal surplusFee=translsRecord.getFee().subtract(dirtUserFee);

       //更改为已结算
        translsRecordService.updateIsSettle(translsRecord.getId(),Settle.IN.settleCode);
        //这个用户总共级数
        int	userCount=0;
        if(StringUtils.isBlank(translsRecord.getUrelationNo())){
            userCount=1;
        }else{
            userCount=translsRecord.getUrelationNo().split("\\.").length;
        }
        //获取用户费率
        UserProfit userProfit=null;
        Long userId=translsRecord.getUserId();
        BigDecimal cost=BigDecimal.ZERO;//成本
        BigDecimal diffRate=BigDecimal.ZERO;//利差
        BigDecimal sumProfit=BigDecimal.ZERO;//总分润金额
        for (int i=0;i<userCount;i++){
            if (userId==0){
                continue;
            }
            userProfit=userProfitService.findByUserId(userId, State.NORMAL.stateCode);
            if(i==0){
                cost=userProfit.getFee();
            }else{
                diffRate=cost.subtract(userProfit.getFee());
                cost=userProfit.getFee();
                BigDecimal diffAmount=surplusFee.multiply(diffRate.divide(new BigDecimal("100"))).setScale(2, RoundingMode.DOWN);
                //直接上级用户除了利差额外获取固定的百分利润
                if( i==1){
                    diffAmount=diffAmount.add(dirtUserFee);
                }

                if (diffRate.compareTo(BigDecimal.ZERO)<=0){
                    continue;
                }
                UserSingleSettle userSingleSettle=new UserSingleSettle();
                userSingleSettle.setSettleDate(StringUtil.getCurrentDateTime("yyyy-MM-dd"));
                userSingleSettle.setAmount(diffAmount);
                userSingleSettle.setSettleStatus(SettleStatus.NOIN.settleCode);
                userSingleSettle.setUrelationNo(userProfit.getUrelationNo());
                userSingleSettle.setTranslsRecordId(translsRecord.getId());
                userSingleSettle.setUserId(userProfit.getUserId());
                userSingleSettleService.addUserSingleSettle(userSingleSettle);
                sumProfit=sumProfit.add(diffAmount);
            }
            userId=userProfit.getUsid();
        }
        //代理商分润orgId=1,为总运营平台
        Long orgId=translsRecord.getOrgId();
        //代理商等级
        int orgCount=translsRecord.getRelationNo().split("\\.").length;
        BigDecimal orgCost=userProfit.getFee();
        OrgProfit orgProfit=null;
        for (int i=0;i<orgCount;i++){
            if(orgId==1){
                continue;
            }
            orgProfit=orgProfitService.findByOrgId(orgId,State.NORMAL.stateCode);
            BigDecimal orgDiffRate=orgCost.subtract(orgProfit.getFee());
            if (orgDiffRate.compareTo(BigDecimal.ZERO)<=0){
                continue;
            }
            BigDecimal orgdiffAmount=surplusFee.multiply(orgDiffRate.divide(new BigDecimal("100"))).setScale(2, RoundingMode.DOWN);
            //顶级用户,利润的百分比给直属代理商
            if(translsRecord.getSid()==0){
                orgdiffAmount=orgdiffAmount.add(dirtUserFee);
            }

            sumProfit=sumProfit.add(orgdiffAmount);
            OrgSignleSettle orgSignleSettle=new OrgSignleSettle();
            orgSignleSettle.setAmount(orgdiffAmount);
            orgSignleSettle.setOrgId(orgProfit.getOrgId());
            orgSignleSettle.setRelationNo(orgProfit.getRelationNo());
            orgSignleSettle.setSettleDate(StringUtil.getCurrentDateTime("yyyy-MM-dd"));
            orgSignleSettle.setSettleStatus(SettleStatus.NOIN.settleCode);
            orgSignleSettle.setTranslsRecordId(translsRecord.getId());
            orgSignleSettleService.addOrgSingleSettle(orgSignleSettle);
            orgCost=orgProfit.getFee();
            orgId=orgProfit.getSid();
        }
        //运营商结算
        BigDecimal businessAmount=translsRecord.getFee().subtract(sumProfit);
        OrgSignleSettle orgSignleSettle=new OrgSignleSettle();
        orgSignleSettle.setAmount(businessAmount);
        orgSignleSettle.setOrgId(new Long(1));
        orgSignleSettle.setRelationNo("1.");
        orgSignleSettle.setSettleDate(StringUtil.getCurrentDateTime("yyyy-MM-dd"));
        orgSignleSettle.setSettleStatus(SettleStatus.NOIN.settleCode);
        orgSignleSettle.setTranslsRecordId(translsRecord.getId());
        orgSignleSettleService.addOrgSingleSettle(orgSignleSettle);

    }

}
