package com.xdream.distribution.service;
import com.xdream.distribution.entity.UserMonthSettle;

   /**
    * tbl_user_month_settle service 接口<br/>
    * 2020-03-11 02:02:19 huangbx
    */ 
public interface IUserMonthSettleService {

      /**
       * 添加用户结算数据
       * @param userMonthSettle
       * @return
       * @throws Exception
       */
   public  Long addUserMonthSettle(UserMonthSettle userMonthSettle) throws Exception;
}

