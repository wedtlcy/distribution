package com.xdream.distribution.service;
import com.xdream.distribution.entity.TranslsRecord;

import java.util.List;

/**
    * tbl_transls_record service 接口<br/>
    * 2020-03-09 02:07:50 huangbx
    */ 
public interface ITranslsRecordService {

      /**
       * 插入流水记录表
       * @param translsRecord 流水记录
       * @return
       * @throws Exception
       */
   public Long insertTranslsRecord(TranslsRecord translsRecord) throws Exception;


       /**
        * 更改流水结算
        * @param translsRecordId
        * @param isSettle
        * @return
        * @throws Exception
        */
   public  int updateIsSettle(Long translsRecordId,String isSettle) throws Exception;


    /**
     * 通过用id统计流水
     * @param settleDate 结算日期
     * @param minUserId  最小用户id
     * @param maxUserId  最高用户id
     * @return
     * @throws Exception
     */
   public List<TranslsRecord> sumAmount(String settleDate,Long minUserId,Long maxUserId) throws Exception;

    /**
     * 通过代理商id统计流水
     * @param settleDate  结算日期
     * @param minOrgId
     * @param maxOrgId
     * @return
     * @throws Exception
     */
   public List<TranslsRecord> sumOrgAmount(String settleDate,Long minOrgId,Long maxOrgId) throws Exception;
}

