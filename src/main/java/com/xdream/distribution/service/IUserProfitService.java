package com.xdream.distribution.service;
import com.xdream.distribution.entity.UserProfit;

import java.util.List;

/**
    * tbl_user_profit service 接口<br/>
    * 2020-03-10 02:44:52 huangbx
    */ 
public interface IUserProfitService {

      /**
       * 获取用户费率
       * @param userId 用户Id
       * @param state  状态
       * @return
       * @throws Exception
       */
      public  UserProfit findByUserId(Long userId,String state) throws  Exception;

       /**
        * 插入用户分润
        * @param userProfit 用户分润
        * @return
        * @throws Exception
        */
      public Long insertUserProfit(UserProfit userProfit) throws Exception;

       /**
        * 统计用户个数
        * @param
        * @return
        * @throws Exception
        */
       public Long countUser() throws Exception;

    /**
     * 分页获取用户
     * @param beginNums
     * @param total
     * @return
     */
       public List<UserProfit>  find(int beginNums,int total);
}

