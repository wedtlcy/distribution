package com.xdream.distribution.service;
import com.xdream.distribution.entity.UserSingleSettle;

   /**
    * tbl_user_signle_settle service 接口<br/>
    * 2020-03-11 09:59:42 huangbx
    */ 
public interface IUserSingleSettleService {

      /**
       * 插入用户结算
       * @param userSingleSettle
       * @return
       * @throws Exception
       */
   public  Long addUserSingleSettle(UserSingleSettle userSingleSettle)throws  Exception;

       /**
        * 统计用户单日总分润金额
        * @param userId  用户id
        * @param settleDate 结算日期
        * @return
        * @throws Exception
        */
   public UserSingleSettle findByUserId(Long userId, String settleDate) throws Exception;
}

