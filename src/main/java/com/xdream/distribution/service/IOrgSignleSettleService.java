package com.xdream.distribution.service;

import com.xdream.distribution.entity.OrgSignleSettle;

/**
    * tbl_org_signle_settle service 接口<br/>
    * 2020-03-12 05:30:02 huangbx
    */ 
public interface IOrgSignleSettleService {

   /**
    * 添加代理商结算
    * @param orgSignleSettle
    * @return
    * @throws Exception
    */
   public Long addOrgSingleSettle (OrgSignleSettle orgSignleSettle) throws Exception;

   /**
    * 统计用户单日总分润金额
    * @param orgId  代理商id
    * @param settleDate 结算日期
    * @return
    * @throws Exception
    */
   public OrgSignleSettle findByOrgId(Long orgId, String settleDate) throws Exception;

}

