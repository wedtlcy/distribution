package com.xdream.distribution.service;

import com.xdream.distribution.entity.TranslsRecord;

public interface IProfitFactory {

    /**
     * 分润工厂
     * @param translsRecord 流水记录
     * @param type  类型
     * @throws Exception
     */
    public void profit(TranslsRecord translsRecord,String type) throws Exception;
}
