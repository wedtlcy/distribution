package com.xdream.distribution.service;

import com.xdream.distribution.entity.TranslsRecord;
import org.springframework.stereotype.Repository;


public interface IPurchaseProfitService {

    /**
     * 分润
     * @param translsRecord  流水记录
     */
    public  void userProfit( TranslsRecord translsRecord ) throws Exception;

}
