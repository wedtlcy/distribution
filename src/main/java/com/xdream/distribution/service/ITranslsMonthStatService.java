package com.xdream.distribution.service;
import com.xdream.distribution.entity.TranslsMonthStat;

   /**
    * tbl_transls_month_stat service 接口<br/>
    * 2020-03-13 03:20:33 huangbx
    */ 
public interface ITranslsMonthStatService {

      /**
       * 添加用户流水月统计
       * @param translsMonthStat
       * @return
       * @throws Exception
       */
   public  Long addTranslsMonthStat(TranslsMonthStat translsMonthStat) throws Exception;
}

