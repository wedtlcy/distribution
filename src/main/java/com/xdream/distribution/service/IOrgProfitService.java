package com.xdream.distribution.service;

import com.xdream.distribution.entity.OrgProfit;

import java.util.List;

/**
    * tbl_org_profit service 接口<br/>
    * 2020-03-12 02:33:07 huangbx
    */ 
public interface IOrgProfitService {

      /**
       * 通过代理商id获取代理商费率
       * @param orgId  代理id
       * @param states  状态
       * @return
       * @throws Exception
       */
   public OrgProfit findByOrgId(Long orgId,String states) throws Exception;

       /**
        * 插入代理商费率表
        * @param orgProfit
        * @return
        * @throws Exception
        */
   public  Long insertOrgProfit(OrgProfit orgProfit) throws Exception;

    /**
     * 统计代理商总数量
     * @return
     * @throws Exception
     */
   public Long countOrg() throws  Exception;

    /**
     * 分页获取代理商
     * @param beginNums
     * @param total
     * @return
     */
    public List<OrgProfit> find(int beginNums, int total);

}

