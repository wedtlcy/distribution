package com.xdream.distribution.service;
import com.xdream.distribution.entity.UserGrade;

   /**
    * tbl_user_grade service 接口<br/>
    * 2020-03-10 02:46:05 huangbx
    */ 
public interface IUserGradeService {

      /**
       * 查找默认等级
       * @param isDefault 默认等级
       * @param state 状态
       * @return
       * @throws Exception
       */
   public  UserGrade findByIsDefault(String isDefault,String state) throws Exception;
}

