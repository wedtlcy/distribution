package com.xdream.distribution.service;

import com.xdream.distribution.entity.OrgDaySettle;

import java.util.List;

/**
    * tbl_org_day_settle service 接口<br/>
    * 2020-03-13 12:54:32 huangbx
    */ 
public interface IOrgDaySettleService {

      /**
       * 代理商日结算
       * @param orgDaySettle
       * @return
       * @throws Exception
       */
   public  Long addOrgDaySettle(OrgDaySettle orgDaySettle) throws Exception;

    /**
     * 获取代理商月统计
     * @param settleDate 统计日期
     * @param minOrgId
     * @param maxOrgId
     * @return
     * @throws Exception
     */
    List<OrgDaySettle> findByDateAndOrgId(String settleDate, Long minOrgId, Long maxOrgId) throws Exception;

    /**
     * 获取代理商日统计数据
     * @param settleDate
     * @param minOrgId
     * @param maxOrgId
     * @return
     * @throws Exception
     */
    List<OrgDaySettle> findByDateSettle(String settleDate, Long minOrgId, Long maxOrgId) throws Exception;

    /**
     * 更改结算状态
     * @param settleDate 结算日期
     * @param minOrgId
     * @param maxOrgId
     * @param settleStatus 结算状态
     * @return
     * @throws Exception
     */
    public int updateOrgDaySettleStatus(String settleDate, Long minOrgId, Long maxOrgId,String settleStatus) throws Exception;
}

