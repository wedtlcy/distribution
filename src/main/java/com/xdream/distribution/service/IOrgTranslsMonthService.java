package com.xdream.distribution.service;
import com.xdream.distribution.entity.OrgTranslsMonth;

   /**
    * tbl_org_transls_month service 接口<br/>
    * 2020-03-13 04:18:36 huangbx
    */ 
public interface IOrgTranslsMonthService {

      /**
       * 新增机构月统计
       * @param orgTranslsMonth 代理商月统计
       * @return
       * @throws Exception
       */
   public Long addOrgTranslsMonth (OrgTranslsMonth orgTranslsMonth) throws Exception;
}

