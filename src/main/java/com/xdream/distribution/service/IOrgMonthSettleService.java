package com.xdream.distribution.service;
import com.xdream.distribution.entity.OrgMonthSettle;

   /**
    * tbl_org_month_settle service 接口<br/>
    * 2020-03-13 05:53:10 huangbx
    */ 
public interface IOrgMonthSettleService {

      /**
       * 添加代理商月分润
       * @param orgMonthSettle
       * @return
       * @throws Exception
       */
   public  Long addOrgMonthSettle(OrgMonthSettle orgMonthSettle) throws Exception;
}

