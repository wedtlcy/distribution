package com.xdream.distribution.service;

import java.math.BigDecimal;

public interface ICalculFeeService {

    /**
     * 计算用户手续费
     * @param fee  总手续费
     * @param userId  用户id
     * @return
     * @throws Exception
     */
    public String calculProfit(BigDecimal fee,Long userId) throws Exception;
}
