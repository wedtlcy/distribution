package com.xdream.distribution.service;
import com.xdream.distribution.entity.OrgTranslsDay;

import java.util.List;

/**
    * tbl_org_transls_day service 接口<br/>
    * 2020-03-13 02:46:16 huangbx
    */ 
public interface IOrgTranslsDayService {

      /**
       * 添加代理商日统计
       * @param orgTranslsDay
       * @return
       * @throws Exception
       */
   public  long addOrgTranslsDay(OrgTranslsDay orgTranslsDay) throws Exception;

    /**
     * 获取代理商月统计
     * @param settleDate 统计日期
     * @param minOrgId
     * @param maxOrgId
     * @return
     * @throws Exception
     */
   List<OrgTranslsDay> findByOrgId(String settleDate,Long minOrgId,Long maxOrgId) throws Exception;
}

