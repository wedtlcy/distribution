package com.xdream.distribution.service;
import com.xdream.distribution.entity.UserDaySettle;

import java.util.List;

/**
    * tbl_user_day_settle service 接口<br/>
    * 2020-03-13 12:05:47 huangbx
    */ 
public interface IUserDaySettleService {

      /**
       * 添加用户日统计
       * @param userDaySettle
       * @return
       * @throws Exception
       */
   public Long addUserDaySettle(UserDaySettle userDaySettle) throws Exception;


    /**
     * 获取用户月统计数据
     * @param settleDate
     * @param minUserId
     * @param maxUserId
     * @return
     * @throws Exception
     */
   public List<UserDaySettle> findByDateAndUserId(String settleDate,Long minUserId,Long maxUserId) throws Exception;


    /**
     * 获取用户日统计数据
     * @param settleDate
     * @param minUserId
     * @param maxUserId
     * @return
     * @throws Exception
     */
    public List<UserDaySettle> findDaySettle(String settleDate,Long minUserId,Long maxUserId) throws Exception;

    /**
     * 更改结算状态
     * @param settleDate 结算日期
     * @param minUserId 用户id
     * @param maxUserId
     * @param settleStatus 结算状态
     * @return
     * @throws Exception
     */
    public int updateSettleStatus(String settleDate,Long minUserId,Long maxUserId,String settleStatus) throws Exception;
}

