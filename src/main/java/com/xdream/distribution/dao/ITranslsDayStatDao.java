package com.xdream.distribution.dao ;
import com.xdream.kernel.dao.IBaseDao;
import com.xdream.distribution.entity.TranslsDayStat;

   /**
    * tbl_transls_day_stat DAO 接口<br/>
    * 2020-03-13 12:11:07 huangbx
    */ 
public interface ITranslsDayStatDao extends IBaseDao<TranslsDayStat, Long>{
}

