package com.xdream.distribution.dao ;
import com.xdream.kernel.dao.IBaseDao;
import com.xdream.distribution.entity.OrgProfit;

   /**
    * tbl_org_profit DAO 接口<br/>
    * 2020-03-12 02:33:07 huangbx
    */ 
public interface IOrgProfitDao extends IBaseDao<OrgProfit, Long>{
}

