package com.xdream.distribution.dao ;
import com.xdream.kernel.dao.IBaseDao;
import com.xdream.distribution.entity.UserDaySettle;

   /**
    * tbl_user_day_settle DAO 接口<br/>
    * 2020-03-13 12:05:47 huangbx
    */ 
public interface IUserDaySettleDao extends IBaseDao<UserDaySettle, Long>{
}

