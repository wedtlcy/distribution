package com.xdream.distribution.dao ;
import com.xdream.kernel.dao.IBaseDao;
import com.xdream.distribution.entity.OrgDaySettle;

   /**
    * tbl_org_day_settle DAO 接口<br/>
    * 2020-03-13 12:54:32 huangbx
    */ 
public interface IOrgDaySettleDao extends IBaseDao<OrgDaySettle, Long>{
}

