package com.xdream.distribution.dao ;
import com.xdream.kernel.dao.IBaseDao;
import com.xdream.distribution.entity.OrgTranslsMonth;

   /**
    * tbl_org_transls_month DAO 接口<br/>
    * 2020-03-13 04:18:36 huangbx
    */ 
public interface IOrgTranslsMonthDao extends IBaseDao<OrgTranslsMonth, Long>{
}

