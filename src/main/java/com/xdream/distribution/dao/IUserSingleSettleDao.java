package com.xdream.distribution.dao ;
import com.xdream.kernel.dao.IBaseDao;
import com.xdream.distribution.entity.UserSingleSettle;

   /**
    * tbl_user_signle_settle DAO 接口<br/>
    * 2020-03-11 09:59:42 huangbx
    */ 
public interface IUserSingleSettleDao extends IBaseDao<UserSingleSettle, Long>{
}

