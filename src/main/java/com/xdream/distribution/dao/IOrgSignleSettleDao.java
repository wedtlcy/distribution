package com.xdream.distribution.dao ;
import com.xdream.kernel.dao.IBaseDao;
import com.xdream.distribution.entity.OrgSignleSettle;

   /**
    * tbl_org_signle_settle DAO 接口<br/>
    * 2020-03-12 05:30:02 huangbx
    */ 
public interface IOrgSignleSettleDao extends IBaseDao<OrgSignleSettle, Long>{
}

