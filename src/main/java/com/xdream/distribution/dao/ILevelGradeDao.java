package com.xdream.distribution.dao ;
import com.xdream.kernel.dao.IBaseDao;
import com.xdream.distribution.entity.LevelGrade;

   /**
    * tbl_level_grade DAO 接口<br/>
    * 2020-03-23 09:56:08 huangbx
    */ 
public interface ILevelGradeDao extends IBaseDao<LevelGrade, Long>{
}

