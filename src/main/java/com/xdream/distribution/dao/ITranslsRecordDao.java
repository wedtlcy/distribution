package com.xdream.distribution.dao ;
import com.xdream.kernel.dao.IBaseDao;
import com.xdream.distribution.entity.TranslsRecord;

   /**
    * tbl_transls_record DAO 接口<br/>
    * 2020-03-09 02:07:50 huangbx
    */ 
public interface ITranslsRecordDao extends IBaseDao<TranslsRecord, Long>{
}

