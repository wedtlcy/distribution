package com.xdream.distribution.dao ;
import com.xdream.kernel.dao.IBaseDao;
import com.xdream.distribution.entity.OrgMonthSettle;

   /**
    * tbl_org_month_settle DAO 接口<br/>
    * 2020-03-13 05:53:10 huangbx
    */ 
public interface IOrgMonthSettleDao extends IBaseDao<OrgMonthSettle, Long>{
}

