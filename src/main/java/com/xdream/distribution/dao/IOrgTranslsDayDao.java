package com.xdream.distribution.dao ;
import com.xdream.kernel.dao.IBaseDao;
import com.xdream.distribution.entity.OrgTranslsDay;

   /**
    * tbl_org_transls_day DAO 接口<br/>
    * 2020-03-13 02:46:16 huangbx
    */ 
public interface IOrgTranslsDayDao extends IBaseDao<OrgTranslsDay, Long>{
}

