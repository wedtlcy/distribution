package com.xdream.distribution.dao ;
import com.xdream.kernel.dao.IBaseDao;
import com.xdream.distribution.entity.UserMonthSettle;

   /**
    * tbl_user_month_settle DAO 接口<br/>
    * 2020-03-11 02:02:19 huangbx
    */ 
public interface IUserMonthSettleDao extends IBaseDao<UserMonthSettle, Long>{
}

