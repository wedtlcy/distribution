package com.xdream.distribution.dao ;
import com.xdream.kernel.dao.IBaseDao;
import com.xdream.distribution.entity.UserProfit;

   /**
    * tbl_user_profit DAO 接口<br/>
    * 2020-03-10 02:44:52 huangbx
    */ 
public interface IUserProfitDao extends IBaseDao<UserProfit, Long>{
}

