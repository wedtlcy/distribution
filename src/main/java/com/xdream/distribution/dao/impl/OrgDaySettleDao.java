package com.xdream.distribution.dao.impl ;
import org.springframework.stereotype.Repository;
import com.xdream.kernel.dao.jdbc.JdbcBaseDaoSupport;
import com.xdream.distribution.entity.OrgDaySettle;
import com.xdream.distribution.dao.IOrgDaySettleDao;

   /**
    * tbl_org_day_settle DAO 接口实现类<br/>
    * 2020-03-13 12:54:32 huangbx
    */ 
@Repository("orgDaySettleDao")
public class OrgDaySettleDao extends JdbcBaseDaoSupport<OrgDaySettle, Long> implements IOrgDaySettleDao {
}

