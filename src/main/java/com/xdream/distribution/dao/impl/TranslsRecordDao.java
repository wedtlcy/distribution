package com.xdream.distribution.dao.impl ;
import org.springframework.stereotype.Repository;
import com.xdream.kernel.dao.jdbc.JdbcBaseDaoSupport;
import com.xdream.distribution.entity.TranslsRecord;
import com.xdream.distribution.dao.ITranslsRecordDao;

   /**
    * tbl_transls_record DAO 接口实现类<br/>
    * 2020-03-09 02:07:50 huangbx
    */ 
@Repository("translsRecordDao")
public class TranslsRecordDao extends JdbcBaseDaoSupport<TranslsRecord, Long> implements ITranslsRecordDao {
}

