package com.xdream.distribution.dao.impl ;
import org.springframework.stereotype.Repository;
import com.xdream.kernel.dao.jdbc.JdbcBaseDaoSupport;
import com.xdream.distribution.entity.OrgTranslsMonth;
import com.xdream.distribution.dao.IOrgTranslsMonthDao;

   /**
    * tbl_org_transls_month DAO 接口实现类<br/>
    * 2020-03-13 04:18:36 huangbx
    */ 
@Repository("orgTranslsMonthDao")
public class OrgTranslsMonthDao extends JdbcBaseDaoSupport<OrgTranslsMonth, Long> implements IOrgTranslsMonthDao {
}

