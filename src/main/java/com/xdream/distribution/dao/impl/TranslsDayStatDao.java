package com.xdream.distribution.dao.impl ;
import org.springframework.stereotype.Repository;
import com.xdream.kernel.dao.jdbc.JdbcBaseDaoSupport;
import com.xdream.distribution.entity.TranslsDayStat;
import com.xdream.distribution.dao.ITranslsDayStatDao;

   /**
    * tbl_transls_day_stat DAO 接口实现类<br/>
    * 2020-03-13 12:11:07 huangbx
    */ 
@Repository("translsDayStatDao")
public class TranslsDayStatDao extends JdbcBaseDaoSupport<TranslsDayStat, Long> implements ITranslsDayStatDao {
}

