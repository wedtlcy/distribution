package com.xdream.distribution.dao.impl ;
import org.springframework.stereotype.Repository;
import com.xdream.kernel.dao.jdbc.JdbcBaseDaoSupport;
import com.xdream.distribution.entity.OrgTranslsDay;
import com.xdream.distribution.dao.IOrgTranslsDayDao;

   /**
    * tbl_org_transls_day DAO 接口实现类<br/>
    * 2020-03-13 02:46:16 huangbx
    */ 
@Repository("orgTranslsDayDao")
public class OrgTranslsDayDao extends JdbcBaseDaoSupport<OrgTranslsDay, Long> implements IOrgTranslsDayDao {
}

