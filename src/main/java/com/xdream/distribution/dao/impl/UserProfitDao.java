package com.xdream.distribution.dao.impl ;
import org.springframework.stereotype.Repository;
import com.xdream.kernel.dao.jdbc.JdbcBaseDaoSupport;
import com.xdream.distribution.entity.UserProfit;
import com.xdream.distribution.dao.IUserProfitDao;

   /**
    * tbl_user_profit DAO 接口实现类<br/>
    * 2020-03-10 02:44:52 huangbx
    */ 
@Repository("userProfitDao")
public class UserProfitDao extends JdbcBaseDaoSupport<UserProfit, Long> implements IUserProfitDao {
}

