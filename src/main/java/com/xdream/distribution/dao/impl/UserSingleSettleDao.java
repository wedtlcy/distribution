package com.xdream.distribution.dao.impl ;
import org.springframework.stereotype.Repository;
import com.xdream.kernel.dao.jdbc.JdbcBaseDaoSupport;
import com.xdream.distribution.entity.UserSingleSettle;
import com.xdream.distribution.dao.IUserSingleSettleDao;

   /**
    * tbl_user_signle_settle DAO 接口实现类<br/>
    * 2020-03-11 09:59:42 huangbx
    */ 
@Repository("userSingleSettleDao")
public class UserSingleSettleDao extends JdbcBaseDaoSupport<UserSingleSettle, Long> implements IUserSingleSettleDao {
}

