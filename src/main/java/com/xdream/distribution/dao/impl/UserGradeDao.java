package com.xdream.distribution.dao.impl ;
import org.springframework.stereotype.Repository;
import com.xdream.kernel.dao.jdbc.JdbcBaseDaoSupport;
import com.xdream.distribution.entity.UserGrade;
import com.xdream.distribution.dao.IUserGradeDao;

   /**
    * tbl_user_grade DAO 接口实现类<br/>
    * 2020-03-10 02:46:05 huangbx
    */ 
@Repository("userGradeDao")
public class UserGradeDao extends JdbcBaseDaoSupport<UserGrade, Long> implements IUserGradeDao {
}

