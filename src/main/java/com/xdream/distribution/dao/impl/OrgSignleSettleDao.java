package com.xdream.distribution.dao.impl ;
import org.springframework.stereotype.Repository;
import com.xdream.kernel.dao.jdbc.JdbcBaseDaoSupport;
import com.xdream.distribution.entity.OrgSignleSettle;
import com.xdream.distribution.dao.IOrgSignleSettleDao;

   /**
    * tbl_org_signle_settle DAO 接口实现类<br/>
    * 2020-03-12 05:30:02 huangbx
    */ 
@Repository("orgSignleSettleDao")
public class OrgSignleSettleDao extends JdbcBaseDaoSupport<OrgSignleSettle, Long> implements IOrgSignleSettleDao {
}

