package com.xdream.distribution.dao.impl ;
import org.springframework.stereotype.Repository;
import com.xdream.kernel.dao.jdbc.JdbcBaseDaoSupport;
import com.xdream.distribution.entity.UserDaySettle;
import com.xdream.distribution.dao.IUserDaySettleDao;

   /**
    * tbl_user_day_settle DAO 接口实现类<br/>
    * 2020-03-13 12:05:47 huangbx
    */ 
@Repository("userDaySettleDao")
public class UserDaySettleDao extends JdbcBaseDaoSupport<UserDaySettle, Long> implements IUserDaySettleDao {
}

