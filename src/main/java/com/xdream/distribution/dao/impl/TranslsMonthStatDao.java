package com.xdream.distribution.dao.impl ;
import org.springframework.stereotype.Repository;
import com.xdream.kernel.dao.jdbc.JdbcBaseDaoSupport;
import com.xdream.distribution.entity.TranslsMonthStat;
import com.xdream.distribution.dao.ITranslsMonthStatDao;

   /**
    * tbl_transls_month_stat DAO 接口实现类<br/>
    * 2020-03-13 03:20:33 huangbx
    */ 
@Repository("translsMonthStatDao")
public class TranslsMonthStatDao extends JdbcBaseDaoSupport<TranslsMonthStat, Long> implements ITranslsMonthStatDao {
}

