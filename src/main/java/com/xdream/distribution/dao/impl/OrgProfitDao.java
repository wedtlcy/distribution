package com.xdream.distribution.dao.impl ;
import org.springframework.stereotype.Repository;
import com.xdream.kernel.dao.jdbc.JdbcBaseDaoSupport;
import com.xdream.distribution.entity.OrgProfit;
import com.xdream.distribution.dao.IOrgProfitDao;

   /**
    * tbl_org_profit DAO 接口实现类<br/>
    * 2020-03-12 02:33:07 huangbx
    */ 
@Repository("orgProfitDao")
public class OrgProfitDao extends JdbcBaseDaoSupport<OrgProfit, Long> implements IOrgProfitDao {
}

