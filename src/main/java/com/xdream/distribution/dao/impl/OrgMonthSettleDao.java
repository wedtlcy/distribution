package com.xdream.distribution.dao.impl ;
import org.springframework.stereotype.Repository;
import com.xdream.kernel.dao.jdbc.JdbcBaseDaoSupport;
import com.xdream.distribution.entity.OrgMonthSettle;
import com.xdream.distribution.dao.IOrgMonthSettleDao;

   /**
    * tbl_org_month_settle DAO 接口实现类<br/>
    * 2020-03-13 05:53:10 huangbx
    */ 
@Repository("orgMonthSettleDao")
public class OrgMonthSettleDao extends JdbcBaseDaoSupport<OrgMonthSettle, Long> implements IOrgMonthSettleDao {
}

