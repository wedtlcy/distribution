package com.xdream.distribution.dao.impl ;
import org.springframework.stereotype.Repository;
import com.xdream.kernel.dao.jdbc.JdbcBaseDaoSupport;
import com.xdream.distribution.entity.LevelGrade;
import com.xdream.distribution.dao.ILevelGradeDao;

   /**
    * tbl_level_grade DAO 接口实现类<br/>
    * 2020-03-23 09:56:08 huangbx
    */ 
@Repository("levelGradeDao")
public class LevelGradeDao extends JdbcBaseDaoSupport<LevelGrade, Long> implements ILevelGradeDao {
}

