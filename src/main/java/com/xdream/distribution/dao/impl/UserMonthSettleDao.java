package com.xdream.distribution.dao.impl ;
import org.springframework.stereotype.Repository;
import com.xdream.kernel.dao.jdbc.JdbcBaseDaoSupport;
import com.xdream.distribution.entity.UserMonthSettle;
import com.xdream.distribution.dao.IUserMonthSettleDao;

   /**
    * tbl_user_month_settle DAO 接口实现类<br/>
    * 2020-03-11 02:02:19 huangbx
    */ 
@Repository("userMonthSettleDao")
public class UserMonthSettleDao extends JdbcBaseDaoSupport<UserMonthSettle, Long> implements IUserMonthSettleDao {
}

