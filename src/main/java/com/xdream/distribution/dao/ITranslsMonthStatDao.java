package com.xdream.distribution.dao ;
import com.xdream.kernel.dao.IBaseDao;
import com.xdream.distribution.entity.TranslsMonthStat;

   /**
    * tbl_transls_month_stat DAO 接口<br/>
    * 2020-03-13 03:20:33 huangbx
    */ 
public interface ITranslsMonthStatDao extends IBaseDao<TranslsMonthStat, Long>{
}

