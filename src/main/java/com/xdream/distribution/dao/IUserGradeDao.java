package com.xdream.distribution.dao ;
import com.xdream.kernel.dao.IBaseDao;
import com.xdream.distribution.entity.UserGrade;

   /**
    * tbl_user_grade DAO 接口<br/>
    * 2020-03-10 02:46:05 huangbx
    */ 
public interface IUserGradeDao extends IBaseDao<UserGrade, Long>{
}

