package com.xdream.distribution.entity;
import com.xdream.kernel.entity.Entity;
import com.xdream.kernel.dao.jdbc.Table;
import java.util.Date;
import java.math.BigDecimal;

   /**
    * tbl_user_day_settle 实体类<br/>
    * 2020-03-13 12:05:47 huangbx
    */ 
@SuppressWarnings("serial")
@Table(name="tbl_user_day_settle")
public class UserDaySettle  extends Entity {
	private Long userId;
	private String urelationNo;
	private String settleDate;
	private BigDecimal amount;
	private Integer transNums;
	private String settleStatus;
	public void setUserId(Long userId){
		this.userId=userId;
	}
	public Long getUserId(){
		return userId;
	}
	public void setUrelationNo(String urelationNo){
		this.urelationNo=urelationNo;
	}
	public String getUrelationNo(){
		return urelationNo;
	}
	public void setSettleDate(String settleDate){
		this.settleDate=settleDate;
	}
	public String getSettleDate(){
		return settleDate;
	}
	public void setAmount(BigDecimal amount){
		this.amount=amount;
	}
	public BigDecimal getAmount(){
		return amount;
	}
	public void setTransNums(Integer transNums){
		this.transNums=transNums;
	}
	public Integer getTransNums(){
		return transNums;
	}
	public void setSettleStatus(String settleStatus){
		this.settleStatus=settleStatus;
	}
	public String getSettleStatus(){
		return settleStatus;
	}

	   @Override
	   public String toString() {
		   return "UserDaySettle{" +
				   "userId=" + userId +
				   ", urelationNo='" + urelationNo + '\'' +
				   ", settleDate='" + settleDate + '\'' +
				   ", amount=" + amount +
				   ", transNums=" + transNums +
				   ", settleStatus='" + settleStatus + '\'' +
				   '}';
	   }
   }

