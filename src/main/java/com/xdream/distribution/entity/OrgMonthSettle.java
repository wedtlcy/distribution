package com.xdream.distribution.entity;
import com.xdream.kernel.entity.Entity;
import com.xdream.kernel.dao.jdbc.Table;
import java.util.Date;
import java.math.BigDecimal;

   /**
    * tbl_org_month_settle 实体类<br/>
    * 2020-03-13 05:53:10 huangbx
    */ 
@SuppressWarnings("serial")
@Table(name="tbl_org_month_settle")
public class OrgMonthSettle  extends Entity {
	private Long orgId;
	private String relationNo;
	private String month;
	private BigDecimal amount;
	private Integer transNums;
	private String settleStatus;
	public void setOrgId(Long orgId){
		this.orgId=orgId;
	}
	public Long getOrgId(){
		return orgId;
	}
	public void setRelationNo(String relationNo){
		this.relationNo=relationNo;
	}
	public String getRelationNo(){
		return relationNo;
	}
	public void setMonth(String month){
		this.month=month;
	}
	public String getMonth(){
		return month;
	}
	public void setAmount(BigDecimal amount){
		this.amount=amount;
	}
	public BigDecimal getAmount(){
		return amount;
	}
	public void setTransNums(Integer transNums){
		this.transNums=transNums;
	}
	public Integer getTransNums(){
		return transNums;
	}
	public void setSettleStatus(String settleStatus){
		this.settleStatus=settleStatus;
	}
	public String getSettleStatus(){
		return settleStatus;
	}

	   @Override
	   public String toString() {
		   return "OrgMonthSettle{" +
				   "orgId=" + orgId +
				   ", relationNo='" + relationNo + '\'' +
				   ", month='" + month + '\'' +
				   ", amount=" + amount +
				   ", transNums=" + transNums +
				   ", settleStatus='" + settleStatus + '\'' +
				   '}';
	   }
   }

