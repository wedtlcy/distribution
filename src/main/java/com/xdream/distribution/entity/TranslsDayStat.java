package com.xdream.distribution.entity;
import com.xdream.kernel.entity.Entity;
import com.xdream.kernel.dao.jdbc.Table;
import java.util.Date;
import java.math.BigDecimal;

   /**
    * tbl_transls_day_stat 实体类<br/>
    * 2020-03-13 12:11:07 huangbx
    */ 
@SuppressWarnings("serial")
@Table(name="tbl_transls_day_stat")
public class TranslsDayStat  extends Entity {
	private String createDate;
	private String createTime;
	private String transDate;
	private Long userId;
	private String urelationNo;
	private BigDecimal amount;
	private Integer num;
	private Long usid;
	public void setCreateDate(String createDate){
		this.createDate=createDate;
	}
	public String getCreateDate(){
		return createDate;
	}
	public void setCreateTime(String createTime){
		this.createTime=createTime;
	}
	public String getCreateTime(){
		return createTime;
	}
	public void setUserId(Long userId){
		this.userId=userId;
	}
	public Long getUserId(){
		return userId;
	}
	public void setUrelationNo(String urelationNo){
		this.urelationNo=urelationNo;
	}
	public String getUrelationNo(){
		return urelationNo;
	}
	public void setAmount(BigDecimal amount){
		this.amount=amount;
	}
	public BigDecimal getAmount(){
		return amount;
	}
	public void setNum(Integer num){
		this.num=num;
	}
	public Integer getNum(){
		return num;
	}
	public void setUsid(Long usid){
		this.usid=usid;
	}
	public Long getUsid(){
		return usid;
	}

	   public String getTransDate() {
		   return transDate;
	   }

	   public void setTransDate(String transDate) {
		   this.transDate = transDate;
	   }
   }

