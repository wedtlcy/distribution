package com.xdream.distribution.entity;
import com.xdream.kernel.entity.Entity;
import com.xdream.kernel.dao.jdbc.Table;
import java.util.Date;
import java.math.BigDecimal;

   /**
    * tbl_user_signle_settle 实体类<br/>
    * 2020-03-11 09:59:42 huangbx
    */ 
@SuppressWarnings("serial")
@Table(name="tbl_user_signle_settle")
public class UserSingleSettle  extends Entity {
	private Long translsRecordId;
	private Long userId;
	private String urelationNo;
	private String settleDate;
	private BigDecimal amount;
	private String settleStatus;
	private Integer  nums;

	public void setTranslsRecordId(Long translsRecord){
		this.translsRecordId=translsRecord;
	}
	public Long getTranslsRecordId(){
		return translsRecordId;
	}
	public void setUserId(Long userId){
		this.userId=userId;
	}
	public Long getUserId(){
		return userId;
	}
	public void setUrelationNo(String urelationNo){
		this.urelationNo=urelationNo;
	}
	public String getUrelationNo(){
		return urelationNo;
	}
	public void setSettleDate(String settleDate){
		this.settleDate=settleDate;
	}
	public String getSettleDate(){
		return settleDate;
	}
	public void setAmount(BigDecimal amount){
		this.amount=amount;
	}
	public BigDecimal getAmount(){
		return amount;
	}
	public String getSettleStatus() {
		   return settleStatus;
	   }
	   public void setSettleStatus(String settleStatus) {
		   this.settleStatus = settleStatus;
	   }

	   public Integer getNums() {
		   return nums;
	   }

	   public void setNums(Integer nums) {
		   this.nums = nums;
	   }
   }

