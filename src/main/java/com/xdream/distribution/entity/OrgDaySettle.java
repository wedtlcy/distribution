package com.xdream.distribution.entity;
import com.xdream.kernel.entity.Entity;
import com.xdream.kernel.dao.jdbc.Table;
import java.util.Date;
import java.math.BigDecimal;

   /**
    * tbl_org_day_settle 实体类<br/>
    * 2020-03-13 12:54:32 huangbx
    */ 
@SuppressWarnings("serial")
@Table(name="tbl_org_day_settle")
public class OrgDaySettle  extends Entity {
	private Long orgId;
	private String relationNo;
	private String settleDate;
	private BigDecimal amount;
	private Integer transNums;
	private String settleStatus;
	public void setOrgId(Long orgId){
		this.orgId=orgId;
	}
	public Long getOrgId(){
		return orgId;
	}
	public void setRelationNo(String relationNo){
		this.relationNo=relationNo;
	}
	public String getRelationNo(){
		return relationNo;
	}
	public void setSettleDate(String settleDate){
		this.settleDate=settleDate;
	}
	public String getSettleDate(){
		return settleDate;
	}
	public void setAmount(BigDecimal amount){
		this.amount=amount;
	}
	public BigDecimal getAmount(){
		return amount;
	}
	public void setTransNums(Integer transNums){
		this.transNums=transNums;
	}
	public Integer getTransNums(){
		return transNums;
	}
	public void setSettleStatus(String settleStatus){
		this.settleStatus=settleStatus;
	}
	public String getSettleStatus(){
		return settleStatus;
	}
}

