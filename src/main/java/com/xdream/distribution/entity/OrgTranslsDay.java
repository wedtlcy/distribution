package com.xdream.distribution.entity;
import com.xdream.kernel.entity.Entity;
import com.xdream.kernel.dao.jdbc.Table;
import java.util.Date;
import java.math.BigDecimal;

   /**
    * tbl_org_transls_day 实体类<br/>
    * 2020-03-13 02:46:16 huangbx
    */ 
@SuppressWarnings("serial")
@Table(name="tbl_org_transls_day")
public class OrgTranslsDay  extends Entity {
	private String createDate;
	private String createTime;
	private Long orgId;
	private String relationNo;
	private BigDecimal amount;
	private Integer num;
	private Long sid;
	private String transDate;
	public void setCreateDate(String createDate){
		this.createDate=createDate;
	}
	public String getCreateDate(){
		return createDate;
	}
	public void setCreateTime(String createTime){
		this.createTime=createTime;
	}
	public String getCreateTime(){
		return createTime;
	}
	public void setOrgId(Long orgId){
		this.orgId=orgId;
	}
	public Long getOrgId(){
		return orgId;
	}
	public void setRelationNo(String relationNo){
		this.relationNo=relationNo;
	}
	public String getRelationNo(){
		return relationNo;
	}
	public void setAmount(BigDecimal amount){
		this.amount=amount;
	}
	public BigDecimal getAmount(){
		return amount;
	}
	public void setNum(Integer num){
		this.num=num;
	}
	public Integer getNum(){
		return num;
	}
	public void setSid(Long sid){
		this.sid=sid;
	}
	public Long getSid(){
		return sid;
	}
	public void setTransDate(String transDate){
		this.transDate=transDate;
	}
	public String getTransDate(){
		return transDate;
	}
}

