package com.xdream.distribution.entity;
import com.xdream.kernel.entity.Entity;
import com.xdream.kernel.dao.jdbc.Table;
import java.util.Date;
import java.math.BigDecimal;

   /**
    * tbl_transls_record 实体类<br/>
    * 2020-03-09 02:07:50 huangbx
    */ 
@SuppressWarnings("serial")
@Table(name="tbl_transls_record")
public class TranslsRecord  extends Entity {
	private String createDate;
	private String createTime;
	private Long translsId;
	private String transDate;
	private String transTime;
	private String orderNo;
	private Long userId;
	private Long usid;
	private String urelationNo;
	private BigDecimal amount;
	private String isSettle;
	private BigDecimal fee;
	private Long orgId;
	private Long sid;
	private  String relationNo;
	private Integer nums;

	public void setCreateDate(String createDate){
		this.createDate=createDate;
	}
	public String getCreateDate(){
		return createDate;
	}
	public void setCreateTime(String createTime){
		this.createTime=createTime;
	}
	public String getCreateTime(){
		return createTime;
	}
	public void setTranslsId(Long translsId){
		this.translsId=translsId;
	}
	public Long getTranslsId(){
		return translsId;
	}
	public void setTransDate(String transDate){
		this.transDate=transDate;
	}
	public String getTransDate(){
		return transDate;
	}
	public void setTransTime(String transTime){
		this.transTime=transTime;
	}
	public String getTransTime(){
		return transTime;
	}
	public void setOrderNo(String orderNo){
		this.orderNo=orderNo;
	}
	public String getOrderNo(){
		return orderNo;
	}
	public void setUserId(Long userId){
		this.userId=userId;
	}
	public Long getUserId(){
		return userId;
	}
	public void setUsid(Long usid){
		this.usid=usid;
	}
	public Long getUsid(){
		return usid;
	}
	public void setUrelationNo(String urelationNo){
		this.urelationNo=urelationNo;
	}
	public String getUrelationNo(){
		return urelationNo;
	}
	public void setAmount(BigDecimal amount){
		this.amount=amount;
	}
	public BigDecimal getAmount(){
		return amount;
	}
	public void setIsSettle(String isSettle){
		this.isSettle=isSettle;
	}
	public String getIsSettle(){
		return isSettle;
	}

	   public BigDecimal getFee() {
		   return fee;
	   }

	   public void setFee(BigDecimal fee) {
		   this.fee = fee;
	   }

	   public Long getOrgId() {
		   return orgId;
	   }

	   public void setOrgId(Long orgId) {
		   this.orgId = orgId;
	   }

	   public Long getSid() {
		   return sid;
	   }

	   public void setSid(Long sid) {
		   this.sid = sid;
	   }

	   public String getRelationNo() {
		   return relationNo;
	   }

	   public void setRelationNo(String relationNo) {
		   this.relationNo = relationNo;
	   }

	   public Integer getNums() {
		   return nums;
	   }

	   public void setNums(Integer nums) {
		   this.nums = nums;
	   }
   }

