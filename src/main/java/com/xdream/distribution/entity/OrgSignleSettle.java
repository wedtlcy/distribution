package com.xdream.distribution.entity;
import com.xdream.kernel.entity.Entity;
import com.xdream.kernel.dao.jdbc.Table;
import java.util.Date;
import java.math.BigDecimal;

   /**
    * tbl_org_signle_settle 实体类<br/>
    * 2020-03-12 05:30:02 huangbx
    */ 
@SuppressWarnings("serial")
@Table(name="tbl_org_signle_settle")
public class OrgSignleSettle  extends Entity {
	private Long translsRecordId;
	private Long orgId;
	private String relationNo;
	private String settleDate;
	private BigDecimal amount;
	private String settleStatus;
	private Integer  nums;
	public void setTranslsRecordId(Long translsRecordId){
		this.translsRecordId=translsRecordId;
	}
	public Long getTranslsRecordId(){
		return translsRecordId;
	}
	public void setOrgId(Long orgId){
		this.orgId=orgId;
	}
	public Long getOrgId(){
		return orgId;
	}
	public void setRelationNo(String relationNo){
		this.relationNo=relationNo;
	}
	public String getRelationNo(){
		return relationNo;
	}
	public void setSettleDate(String settleDate){
		this.settleDate=settleDate;
	}
	public String getSettleDate(){
		return settleDate;
	}
	public void setAmount(BigDecimal amount){
		this.amount=amount;
	}
	public BigDecimal getAmount(){
		return amount;
	}
	public void setSettleStatus(String settleStatus){
		this.settleStatus=settleStatus;
	}
	public String getSettleStatus(){
		return settleStatus;
	}

	   public Integer getNums() {
		   return nums;
	   }

	   public void setNums(Integer nums) {
		   this.nums = nums;
	   }
   }

