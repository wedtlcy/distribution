package com.xdream.distribution.entity;
import com.xdream.kernel.entity.Entity;
import com.xdream.kernel.dao.jdbc.Table;
import java.util.Date;
import java.math.BigDecimal;

   /**
    * tbl_org_profit 实体类<br/>
    * 2020-03-12 02:33:07 huangbx
    */ 
@SuppressWarnings("serial")
@Table(name="tbl_org_profit")
public class OrgProfit  extends Entity {
	private Long orgId;
	private String createDate;
	private String createTime;
	private String relationNo;
	private BigDecimal fee;
	private Long sid;
	private String state;
	public void setOrgId(Long orgId){
		this.orgId=orgId;
	}
	public Long getOrgId(){
		return orgId;
	}
	public void setCreateDate(String createDate){
		this.createDate=createDate;
	}
	public String getCreateDate(){
		return createDate;
	}
	public void setCreateTime(String createTime){
		this.createTime=createTime;
	}
	public String getCreateTime(){
		return createTime;
	}
	public void setRelationNo(String relationNo){
		this.relationNo=relationNo;
	}
	public String getRelationNo(){
		return relationNo;
	}
	public void setFee(BigDecimal fee){
		this.fee=fee;
	}
	public BigDecimal getFee(){
		return fee;
	}
	public void setSid(Long sid){
		this.sid=sid;
	}
	public Long getSid(){
		return sid;
	}
	public void setState(String state){
		this.state=state;
	}
	public String getState(){
		return state;
	}
}

