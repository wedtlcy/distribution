package com.xdream.distribution.entity;
import com.xdream.kernel.entity.Entity;
import com.xdream.kernel.dao.jdbc.Table;
import java.util.Date;
import java.math.BigDecimal;

   /**
    * tbl_user_profit 实体类<br/>
    * 2020-03-10 02:44:52 huangbx
    */ 
@SuppressWarnings("serial")
@Table(name="tbl_user_profit")
public class UserProfit  extends Entity {
	private Long userId;
	private Long userGradeId;
	private String urelationNo;
	private BigDecimal fee;
	private Long usid;
	private String state;
	private String createDate;
	private String createTime;

	public void setUserId(Long userId){
		this.userId=userId;
	}
	public Long getUserId(){
		return userId;
	}
	public void setUserGradeId(Long userGradeId){
		this.userGradeId=userGradeId;
	}
	public Long getUserGradeId(){
		return userGradeId;
	}
	public void setUrelationNo(String urelationNo){
		this.urelationNo=urelationNo;
	}
	public String getUrelationNo(){
		return urelationNo;
	}
	public void setFee(BigDecimal fee){
		this.fee=fee;
	}
	public BigDecimal getFee(){
		return fee;
	}
	public void setUsid(Long usid){
		this.usid=usid;
	}
	public Long getUsid(){
		return usid;
	}
	public void setState(String state){
		this.state=state;
	}
	public String getState(){
		return state;
	}

	   public String getCreateDate() {
		   return createDate;
	   }

	   public void setCreateDate(String createDate) {
		   this.createDate = createDate;
	   }

	   public String getCreateTime() {
		   return createTime;
	   }

	   public void setCreateTime(String createTime) {
		   this.createTime = createTime;
	   }
   }

