package com.xdream.distribution.entity;
import com.xdream.kernel.entity.Entity;
import com.xdream.kernel.dao.jdbc.Table;
import java.util.Date;
import java.math.BigDecimal;

   /**
    * tbl_level_grade 实体类<br/>
    * 2020-03-23 09:56:08 huangbx
    */ 
@SuppressWarnings("serial")
@Table(name="tbl_level_grade")
public class LevelGrade  extends Entity {
	private Integer level;
	private String levelName;
	private BigDecimal levelRate;
	private String state;
	public void setLevel(Integer level){
		this.level=level;
	}
	public Integer getLevel(){
		return level;
	}
	public void setLevelName(String levelName){
		this.levelName=levelName;
	}
	public String getLevelName(){
		return levelName;
	}
	public void setLevelRate(BigDecimal levelRate){
		this.levelRate=levelRate;
	}
	public BigDecimal getLevelRate(){
		return levelRate;
	}
	public void setState(String state){
		this.state=state;
	}
	public String getState(){
		return state;
	}
}

