package com.xdream.distribution.entity;
import com.xdream.kernel.entity.Entity;
import com.xdream.kernel.dao.jdbc.Table;
import java.util.Date;
import java.math.BigDecimal;

   /**
    * tbl_user_grade 实体类<br/>
    * 2020-03-10 02:46:05 huangbx
    */ 
@SuppressWarnings("serial")
@Table(name="tbl_user_grade")
public class UserGrade  extends Entity {
	private String userType;
	private String userTypeName;
	private BigDecimal userRate;
	private String isDefault;
	private String state;
	public void setUserType(String userType){
		this.userType=userType;
	}
	public String getUserType(){
		return userType;
	}
	public void setUserTypeName(String userTypeName){
		this.userTypeName=userTypeName;
	}
	public String getUserTypeName(){
		return userTypeName;
	}
	public void setUserRate(BigDecimal userRate){
		this.userRate=userRate;
	}
	public BigDecimal getUserRate(){
		return userRate;
	}
	public void setIsDefault(String isDefault){
		this.isDefault=isDefault;
	}
	public String getIsDefault(){
		return isDefault;
	}
	public void setState(String state){
		this.state=state;
	}
	public String getState(){
		return state;
	}
}

