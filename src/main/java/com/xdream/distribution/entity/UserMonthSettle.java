package com.xdream.distribution.entity;
import com.xdream.kernel.entity.Entity;
import com.xdream.kernel.dao.jdbc.Table;
import java.util.Date;
import java.math.BigDecimal;

   /**
    * tbl_user_month_settle 实体类<br/>
    * 2020-03-11 02:02:19 huangbx
    */ 
@SuppressWarnings("serial")
@Table(name="tbl_user_month_settle")
public class UserMonthSettle  extends Entity {
	private Long userId;
	private String urelationNo;
	private String month;
	private BigDecimal amount;
	private Integer transNums;
	private String settleStatus;
	public void setUserId(Long userId){
		this.userId=userId;
	}
	public Long getUserId(){
		return userId;
	}
	public void setUrelationNo(String urelationNo){
		this.urelationNo=urelationNo;
	}
	public String getUrelationNo(){
		return urelationNo;
	}
	public void setMonth(String month){
		this.month=month;
	}
	public String getMonth(){
		return month;
	}
	public void setAmount(BigDecimal amount){
		this.amount=amount;
	}
	public BigDecimal getAmount(){
		return amount;
	}
	public void setTransNums(Integer transNums){
		this.transNums=transNums;
	}
	public Integer getTransNums(){
		return transNums;
	}

	   public String getSettleStatus() {
		   return settleStatus;
	   }

	   public void setSettleStatus(String settleStatus) {
		   this.settleStatus = settleStatus;
	   }
   }

