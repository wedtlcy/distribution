package com.xdream.distribution.entity;
import com.xdream.kernel.entity.Entity;
import com.xdream.kernel.dao.jdbc.Table;
import java.util.Date;
import java.math.BigDecimal;

   /**
    * tbl_org_transls_month 实体类<br/>
    * 2020-03-13 04:18:36 huangbx
    */ 
@SuppressWarnings("serial")
@Table(name="tbl_org_transls_month")
public class OrgTranslsMonth  extends Entity {
	private String monthYear;
	private Long orgId;
	private String relationNo;
	private BigDecimal amount;
	private Integer num;
	private Long sid;
	public void setMonthYear(String monthYear){
		this.monthYear=monthYear;
	}
	public String getMonthYear(){
		return monthYear;
	}
	public void setOrgId(Long orgId){
		this.orgId=orgId;
	}
	public Long getOrgId(){
		return orgId;
	}
	public void setRelationNo(String relationNo){
		this.relationNo=relationNo;
	}
	public String getRelationNo(){
		return relationNo;
	}
	public void setAmount(BigDecimal amount){
		this.amount=amount;
	}
	public BigDecimal getAmount(){
		return amount;
	}
	public void setNum(Integer num){
		this.num=num;
	}
	public Integer getNum(){
		return num;
	}
	public void setSid(Long sid){
		this.sid=sid;
	}
	public Long getSid(){
		return sid;
	}
}

