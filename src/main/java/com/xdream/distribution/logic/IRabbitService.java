package com.xdream.distribution.logic;

public interface IRabbitService {

    /**
     * 发送消息队列
     * @param json  消息主体
     * @param queues 队列名
     */
    public void send(String json,String queues);
}
