package com.xdream.distribution.logic;

import com.xdream.distribution.entity.OrgProfit;

public interface IOrgLogicService {

    /**
     * 插入代理商费率
     * @param orgProfit 代理商费率
     * @return
     * @throws Exception
     */
    public String insertOrgLogic(OrgProfit orgProfit,String uuid) throws Exception;

}
