package com.xdream.distribution.logic.impl;


import com.xdream.distribution.logic.IRabbitService;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service("rabbitService")
public class RabbitService implements IRabbitService {
    @Autowired
    private AmqpTemplate payRabbit;

    /**
     * 发送消息队列
     * @param json  消息主体
     * @param queues 队列名
     */
    public void send(String json,String queues){
        MessageProperties messageProperties=new MessageProperties();
        int random=(int)(Math.random()*10);
        String messageId= UUID.randomUUID().toString().replaceAll("-", "")+random;
        messageProperties.setMessageId(messageId);
        messageProperties.setContentEncoding("UTF-8");
        Message msg=new Message(json.getBytes(), messageProperties);
        payRabbit.send("", queues,msg );
    }

}
