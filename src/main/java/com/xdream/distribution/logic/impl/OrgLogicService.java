package com.xdream.distribution.logic.impl;

import com.xdream.distribution.entity.OrgProfit;
import com.xdream.distribution.enums.State;
import com.xdream.distribution.logic.IOrgLogicService;
import com.xdream.distribution.service.IOrgProfitService;
import com.xdream.distribution.utils.ResultUtil;
import com.xdream.kernel.util.StringUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("orgLogicService")
public class OrgLogicService implements IOrgLogicService{

    @Resource(name="orgProfitService")
    private IOrgProfitService orgProfitService;
    /**
     * 插入代理商费率
     * @param orgProfit 代理商费率
     * @return
     * @throws Exception
     */
    public String insertOrgLogic(OrgProfit orgProfit,String uuid) throws Exception {
        //返回结果json
        String resultJson;
        //先判断数据库是否存在这条记录
        OrgProfit original = orgProfitService.findByOrgId(orgProfit.getOrgId(), State.NORMAL.stateCode);
        if(original !=null){
            return ResultUtil.sendJson("fail","代理商已存在",null,null);
        }
        //判断是不是总平台添加，总平台默认orgId=1
        if(0!=orgProfit.getSid()){
           //先获取上级的代理商费率
            OrgProfit superProfit = orgProfitService.findByOrgId(orgProfit.getSid(),State.NORMAL.stateCode);
            if(orgProfit.getFee().compareTo(superProfit.getFee())<0){
                return ResultUtil.sendJson("fail","代理商费率设置过低",null,null);
            }
        }
        String createDate = StringUtil.getCurrentDateTime("yyyy-MM-dd");
        String createTime = StringUtil.getCurrentDateTime("HH:mm:ss");
        orgProfit.setCreateDate(createDate);
        orgProfit.setCreateTime(createTime);
        orgProfit.setState(State.NORMAL.stateCode);
        Long orgProfitId = orgProfitService.insertOrgProfit(orgProfit);
        if (orgProfitId >0){
            return ResultUtil.sendJson("success","代理商添加成功",null,null);
        }else {
            return ResultUtil.sendJson("fail","代理商添加失败",null,null);
        }

    }
}
